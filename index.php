<?php 

include_once('utility/Header.php');
?>

<!DOCTYPE html>

<html>
    <head>

        <?php loadingHtmlNavbar::loadHeadContent(); ?>
        
    </head>

    <body>

        <?php loadingHtmlNavbar::loadNavbar(1); ?>

        <?php loadingHtmlIndex::loadIntroductoryText(); ?>

    </body>
</html>
