<?php

include_once('utility/Header.php');
ManagerSession::start();

$isLogged = ManagerSession::isLogged();

if (!$isLogged) // Il doit nécéssairement être connecté
{
    return;
}

$user_id = $_SESSION['user_id'];

$get_permissions_grade = $_SESSION['instanceMysql']->getPermissionsGradeByUserID($user_id);

if ($get_permissions_grade != 1) // Enseignant uniquement
{
    return;
}

$state_code = -1;

if (isset($_POST['downloadRenderingHomework']))
{
    LoadingHtmlHomeworksManager::downloadRenderingHomework($_POST['downloadRenderingHomework']);
}
else if (isset($_POST['deleteHomework']))
{
    $state_code = LoadingHtmlHomeworksManager::deleteHomework($_POST['deleteHomework']);
}

?>

<!DOCTYPE html>

<html>
    <head>

        <?php loadingHtmlNavbar::loadHeadContent(); ?>

    </head>

    <body>

        <?php loadingHtmlNavbar::loadNavbar(4); ?>

        <?php loadingHtmlHomeworksManager::displayHomeworks($state_code); ?>

    </body>

</html>