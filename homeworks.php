<?php

include_once('utility/Header.php');
ManagerSession::start();

$isLogged = ManagerSession::isLogged();

if (!$isLogged) // Il doit nécéssairement être connecté
{
    return;
}

$user_id = $_SESSION['user_id'];

$get_permissions_grade = $_SESSION['instanceMysql']->getPermissionsGradeByUserID($user_id);

if ($get_permissions_grade != 0) // Elève uniquement
{
    return;
}

$list_homeworks = $_SESSION['instanceMysql']->getHomeworks($user_id);

$upload_homework_code = -1;

if (isset($_FILES['fileHomework']) && isset($_POST['codage']))
{
    $codage = $_POST['codage'];

    if ($_SESSION['instanceMysql']->checkCodageHomeworks($codage, $user_id))
    {
        if (!$_SESSION['instanceFtp']->homeworkAlreadySent($codage, $user_id))
        {
            $upload_homework_code = $_SESSION['instanceFtp']->addHomework($codage, $user_id, $_FILES['fileHomework']['name'], $_FILES['fileHomework']['size'], $_FILES['fileHomework']['tmp_name']);
        }
    }
}
else if (isset($_POST['downloadHomework']))
{
    loadingHtmlHomeworks::downloadHomework($_POST['downloadHomework']);
}
else if (isset($_POST['deleteHomework']))
{
    loadingHtmlHomeworks::deleteHomework($_POST['deleteHomework']);
}
?>

<!DOCTYPE html>

<html>
    <head>

        <?php loadingHtmlNavbar::loadHeadContent(); ?>
        
    </head>

    <body>
    
        <?php loadingHtmlNavbar::loadNavbar(3); ?>
        <?php loadingHtmlHomeworks::loadIntroducingText($upload_homework_code); ?>
        <?php loadingHtmlHomeworks::addListingHomeworks($_SESSION['user_id'], $list_homeworks); ?>

    </body>

</html>