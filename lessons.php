<?php

include_once('utility/Header.php');

define('START_PATH', '/Cours');

ManagerSession::start();

$isLogged = ManagerSession::isLogged();

if (!$isLogged) // Il doit nécessairement être connecté
{
    return;
}

$get_permissions_grade = $_SESSION['instanceMysql']->getPermissionsGradeByUserID($_SESSION['user_id']);

if ($get_permissions_grade != 0 & $get_permissions_grade != 1) // Elève ou Professeur
{
    return;
}

$is_teacher = $_SESSION['isTeacher'];

$state_code = 0;

if (!isset($_SESSION['path_lessons']))
{
    $_SESSION['path_lessons'] = START_PATH;
}

if (isset($_POST['Path_Next']))
{
    LoadingHtmlLessons::appendFolderInPath($_POST['Path_Next']);
}
else if (isset($_POST['Path_Back']))
{
    LoadingHtmlLessons::reduceFolderInPath($_POST['Path_Back']);
}
else if (isset($_POST['Path_Delete']) && $is_teacher)
{
    $state_code = $_SESSION['instanceFtp']->deleteByPath($_POST['Path_Delete']);
}
else if (isset($_POST['Create_Folder']) && $is_teacher)
{
    $current_path = $_SESSION['path_lessons'];
    $state_code = $_SESSION['instanceFtp']->createFolder($current_path . "/" . $_POST['Create_Folder']);
}
else if (isset($_FILES['Fichier_Upload']) && $is_teacher)
{
    $state_code = $_SESSION['instanceFtp']->uploadFileOnServerSide($_FILES['Fichier_Upload']["name"], $_FILES['Fichier_Upload']["size"], $_FILES['Fichier_Upload']["tmp_name"]);
}

?>

<!DOCTYPE html>

<html>
    <head>

        <?php loadingHtmlNavbar::loadHeadContent(); ?>
        <link href="css/filepicker.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="js/filepicker.js"></script>
        
    </head>

    <body>

        <?php loadingHtmlNavbar::loadNavbar(2); ?>

        <ol style="margin-top:-20px;" class="breadcrumb">
            <?php LoadingHtmlLessons::infosPath(); ?>
        </ol>

        <div class="list-group">
            <?php LoadingHtmlLessons::displayPath(); ?>
        </div>
        
        <?php LoadingHtmlLessons::displayStateCode($state_code); ?>

        <?php LoadingHtmlLessons::loadServiceCreateFiles(); ?>

    </body>

</html>