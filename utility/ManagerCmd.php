<?php

include_once('Header.php');

define('DATABASE_ERROR', -1);

// add_user
define('USERNAME_INPUT_ERROR', 1);
define('PASSWORD_INPUT_ERROR', 2);
define('GRADE_INPUT_ERROR', 3);
define('USERNAME_INPUT_EXISTS', 4);
define('ADD_USER_OK', 5);
define('ADD_USER_SUCCESS', 6);
define('ADD_USER_ERROR', 7);

// remove_user
define('USERNAME_NOT_EXISTS', 8);
define('GRADE_USER_ERROR', 9);
define('REMOVE_USER_SUCCESS', 10);

// add_class
define('MAX_LENGTH_CLASS_NAME', 20);

define('CLASS_NAME_EXISTS', 11);
define('ADD_CLASS_SUCCESS', 12);
define('ADD_CLASS_FTP_ERROR', 13);
define('FILL_CLASS_FTP_ERROR', 14);
define('ADD_CLASS_FTP_SUCCESS', 15);

// add_lesson
define('MAX_LENGTH_LESSON_NAME', 20);

define('LESSON_NAME_EXISTS', 16);
define('ADD_LESSON_SUCCESS', 17);
define('ADD_LESSON_FTP_ERROR', 18);
define('ADD_LESSON_FTP_SUCCESS', 19);

// add_user_class_lesson
define('ADD_USER_CLASS_LESSON_SUCCESS', 20);
define('ADD_USER_CLASS_LESSON_FAILED', 21);

class ManagerCmd
{
    /**
     * Traite la commande 'add_user'
     */
    public static function add_user($username, $password, $grade)
    {
        $state_code = self::checkAddUser($username, $password, $grade);

        switch ($state_code)
        {
            case USERNAME_INPUT_ERROR:
                echo "danger Le nom d'utilisateur entré est incorrect.";
                break;
            case PASSWORD_INPUT_ERROR:
                echo "danger Le mot de passe entré est incorrect.";
                break;
            case GRADE_INPUT_ERROR:
                echo "danger Le grade entré est incorrect.";
                break;
            case ADD_USER_OK:
                switch ($_SESSION['instanceMysql']->addUser($username, $password, $grade))
                {
                    case USERNAME_INPUT_EXISTS:
                        echo "danger Le nom d'utilisateur a déjà été choisi";
                        break;
                    case ADD_USER_SUCCESS:
                        echo "success L'utilisateur : $username a été créé avec le grade $grade dans la base de données !";
                        break;
                    case ADD_USER_ERROR:
                        echo "danger Impossible de créer l'utilisateur $username !";
                        break;
                    case DATABASE_ERROR:
                        echo "danger La base de données est inaccessible.";
                        break;
                }
        }
    }

    /**
     * Vérifie si les informations entrés en ligne de commande sont corrects.
     */
    public static function checkAddUser($username, $password, $grade)
    {
        if (!$_SESSION['instanceMysql']->checkUsername($username))
        {
            return USERNAME_INPUT_ERROR;
        }

        if (!$_SESSION['instanceMysql']->checkPassword($password))
        {
            return PASSWORD_INPUT_ERROR;
        }

        if ($grade != 0 && $grade != 1)
        {
            return GRADE_INPUT_ERROR;
        }

        return ADD_USER_OK;
    }

    /**
     * Gère la commande 'remove_user'
     */
    public static function remove_user($username)
    {
        switch ($_SESSION['instanceMysql']->removeUser($username))
        {
            case USERNAME_NOT_EXISTS:
                echo "danger L'utilisateur $username n'existe pas.";
                break;
            case GRADE_USER_ERROR:
                echo "danger Vous ne pouvez supprimer que les étudiants et les enseignants.";
                break;
            case REMOVE_USER_SUCCESS:
                echo "success L'utilisateur $username a été supprimé de la base de données!";
                break;
            case DATABASE_ERROR:
                echo "danger La base de données est inaccessible.";
                break;
        }
    }

    /**
     * Gère la commande 'add_class'
     */
    public static function add_class($class_name_array)
    {
        $class_name = self::checkClassName($class_name_array);
        if ($class_name == "")
        {
            echo "danger La taille maximale d'un nom de classe est de " . MAX_LENGTH_CLASS_NAME . ".";
            return;
        }

        switch ($_SESSION['instanceMysql']->addClass($class_name))
        {
            case CLASS_NAME_EXISTS:
                echo "danger Le nom de classe est déjà utilisé.";
                break;
            case ADD_CLASS_SUCCESS:
                echo "success La classe $class_name a été ajouté à la base de données!";
                switch($_SESSION['instanceFtp']->createDirectoryClass($class_name))
                {
                    case ADD_CLASS_FTP_ERROR:
                        echo "|danger Le dossier de cours n'a pas pu être ajouté. Annulation.";
                        $_SESSION['instanceMysql']->deleteClass($class_name);
                        break;
                    case FILL_CLASS_FTP_ERROR:
                        echo "|danger Impossible de remplir le dossier de cours avec les matières. Annulation.";
                        $_SESSION['instanceMysql']->deleteClass($class_name);
                        break;
                    case ADD_CLASS_FTP_SUCCESS:
                        echo "|success Le dossier de cours a été créé !";
                        break;
                }
                break;
            case DATABASE_ERROR:
                echo "danger La base de données est inaccessible.";
                break;
        }
    }

    /**
     * Reforme le nom de la classe et vérifie si sa taille est correct.
     */
    public static function checkClassName($class_name_array)
    {
        $class_name = "";
        for ($i = 0; $i < count($class_name_array); $i++)
        {
            $class_name .= $class_name_array[$i];
            if ($i != count ($class_name_array) - 1)
                $class_name .= " ";
        }

        if (strlen($class_name) <= 0 || strlen($class_name) > MAX_LENGTH_CLASS_NAME)
        {
            return "";
        }

        return $class_name;
    }

    /**
     * Gère la commande 'add_lesson'
     */
    public static function add_lesson($lesson_name_array)
    {
        $lesson_name = self::checkLessonName($lesson_name_array);
        if ($lesson_name == "")
        {
            echo "danger La taille maximale d'une matière est de " . MAX_LENGTH_CLASS_NAME . ".";
            return;
        }

        switch ($_SESSION['instanceMysql']->addLesson($lesson_name))
        {
            case LESSON_NAME_EXISTS:
                echo "danger Le nom de matière est déjà utilisé.";
                break;
            case ADD_LESSON_SUCCESS:
                echo "success La matière $lesson_name a été ajouté !";

                switch ($_SESSION['instanceFtp']->fillAllDirectoryClassWithLesson($lesson_name))
                {
                    case ADD_LESSON_FTP_ERROR:
                        echo "|danger Impossible de remplir les dossiers de classe avec cette matière. Annulation.";
                        $_SESSION['instanceMysql']->deleteLesson($lesson_name);
                        $_SESSION['instanceFtp']->deleteAllDirectoryLesson($lesson_name);
                        break;
                    case ADD_LESSON_FTP_SUCCESS:
                        echo "|success Les dossiers de la matière $lesson_name ont été ajouté dans toutes les classes.";
                        break;
                }
                break;
            case DATABASE_ERROR:
                echo "danger La base de données est inaccessible.";
                break;
        }
    }

    /**
     * Reforme le nom de la matière et vérifie si sa taille est correct.
     */
    public static function checkLessonName($lesson_name_array)
    {
        $lesson_name = "";
        for ($i = 0; $i < count($lesson_name_array); $i++)
        {
            $lesson_name .= $lesson_name_array[$i];
            if ($i != count ($lesson_name_array) - 1)
                $lesson_name .= " ";
        }

        if (strlen($lesson_name) <= 0 || strlen($lesson_name) > MAX_LENGTH_LESSON_NAME)
        {
            return "";
        }

        return $lesson_name;
    }

    /**
     * Gère la commande 'show_users'
     * Affiche le nom, le grade ainsi que les identifiants de tous les utilisateurs
     */
    public static function show_users()
    {
        $array_users_data = $_SESSION['instanceMysql']->get_users_data();

        switch ($array_users_data)
        {
            case DATABASE_ERROR:
                echo "danger Impossible de communiquer avec la base de données";
                break;
            default:
            echo "default ";
?>
            
            <table style="font-size:13px;" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <td>user_id</th>
                        <td>username</th>
                        <td>permissions_grade</th>
                    </tr>
                </thead>
                <tbody>
<?php
        	
                foreach ($array_users_data as $user_data)
                {
		    echo "
			<tr>
                           <td>" . $user_data['user_id'] . "</th>
                           <td>" . $user_data['username'] . "</td>
                           <td>" . $user_data['permissions_grade'] . "</td>
			</tr>";
                }
                echo "</tbody>
                   </table>";
                break;
        }
    }

    /**
     * Gère la commande 'show_class'
     * Affiche l'identifiant et le nom de chaque classe
     */
    public static function show_class()
    {
        $array_class = $_SESSION['instanceMysql']->get_class();

        switch ($array_class)
        {
            case DATABASE_ERROR:
                echo "danger Impossible de communiquer avec la base de données";
                break;
            default:
            echo "default ";
?>
             <table style="font-size:13px;" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <td>class_id</th>
                        <td>class_name</th>
                    </tr>
                </thead>
                <tbody>
<?php
                foreach ($array_class as $class)
                {
		    echo "
			<tr>
                           <td>" . $class['class_id'] . "</td>
                           <td>" . $class['class_name'] . "</td>
			</tr>";
                }
                echo "</tbody>
                   </table>";
                break;
        }
    }

    /**
     * Gère la commande 'show_lessons'
     * Affiche l'identifiant et le nom de chaque matière
     */
    public static function show_lessons()
    {
        $array_lessons = $_SESSION['instanceMysql']->get_lessons();

        switch ($array_lessons)
        {
            case DATABASE_ERROR:
                echo "danger Impossible de communiquer avec la base de données";
                break;
            default:
             echo "default ";
?>
            <table style="font-size:13px;" class="table table-striped table-hover">
                <thead>
                    <tr>
                        <td>lesson_id</td>
                        <td>lesson_name</td>
                    </tr>
                </thead>
                <tbody>
<?php
                foreach ($array_lessons as $lesson)
                {
		    echo "
			<tr>
                           <td>" . $lesson['lesson_id'] . "</td>
                           <td>" . $lesson['lesson_name'] . "</td>
			</tr>";
                }
                echo "</tbody>
                   </table>";
                break;
        }
    }

    /**
     * Gère la commande 'add_user_class_lesson'
     * Fait correspondre à une utilisateur une classe et une matière
     */
    public static function add_user_class_lesson($user_id, $class_id, $lesson_id)
    {
         switch ($_SESSION['instanceMysql']->add_user_class_lesson($user_id, $class_id, $lesson_id))
         {
              case DATABASE_ERROR:
                  echo "danger La base de données est inaccessible.";
                  break;
              case ADD_USER_CLASS_LESSON_FAILED:
                  echo "danger Impossible d'ajouter cette relation. Vérifiez les informations entrées.";
                  break;
              case ADD_USER_CLASS_LESSON_SUCCESS:
                  echo "success La relation a été ajouté !";
                  break;
         }
    }
}

?>	