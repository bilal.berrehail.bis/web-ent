<?php
include_once('Header.php');

define('SERVER_MYSQL', 'devbrlcobk126870.mysql.db');
define('USER_MYSQL', 'devbrlcobk126870');
define('PASSWORD_MYSQL', 'ffWQuQvFM69kf');
define('DATABASE_MYSQL', 'devbrlcobk126870');

define('POST_VARIABLES_ERROR', 200);
define('CONNECTION_MYSQL_ERROR', 201);
define('SESSION_ERROR', 202);

class ManagerMysql
{
    private $connection;

    /**
     * Constructeur de la classe
     */
    function __construct()
    {
        $this->openConnection();
    }

    /**
     * Récupère l'unique instance de la classe
     */
    public static function getInstance()
    {
        return new ManagerMysql();
    }

    /**
     * Ouverture d'une connexion au serveur MySQL
     */
    public function openConnection()
    {
        $this->$connection = mysqli_connect(SERVER_MYSQL, USER_MYSQL, PASSWORD_MYSQL, DATABASE_MYSQL);

        if (mysqli_connect_errno())
        {
            return CONNECTION_MYSQL_ERROR;
        }

        $query = $this->$connection->prepare('SET NAMES utf8');
        $query->execute();
    }

    /**
     * Vérification que l'identifiant et le mot de passe respecte les conditions d'une bonne chaine de caractères
     */
    public function checkGoodInformations($input_username, $input_password)
    {
        return ($this->checkUsername($input_username) && $this->checkPassword($input_password));
    }

    /**
     * Vérification que l'identifiant possède une chaine de caractères correct
     */
    public function checkUsername($input_username)
    {
        $result = preg_match('/[A-Za-z0-9-.\'çéèëäïà]{' . MIN_SIZE_USERNAME . ',' . MAX_SIZE_USERNAME . '}/', $input_username, $matches, PREG_UNMATCHED_AS_NULL);

        return !strcmp($input_username, $matches[0]) && $result;
    }

    /**
     * Vérification si le mot de passe possède une taille correct
     */
    public function checkPassword($input_password)
    {
        $result = preg_match('/[A-Za-z0-9-.\'çéèëäïà]{' . MIN_SIZE_PASSWORD . ',' . MAX_SIZE_PASSWORD . '}/', $input_password, $matches, PREG_UNMATCHED_AS_NULL);
        
        return !strcmp($input_password, $matches[0]) && $result;
    }

    /**
     * Vérification des identifiants
     */
    public function checkMethodPostLogin($input_username, $input_password, &$user_id)
    {
        if (!ManagerSession::isStart())
        {
            return SESSIONS_ERROR;
        }

        if (ManagerSession::isLogged())
        {
            return SESSIONS_ERROR;
        }

        if (!$this->checkGoodInformations($input_username, $input_password))
        {
            return POST_VARIABLES_ERROR;
        }

        if (!empty($input_username) && !empty($input_password))
        {
            $input_username = $this->htmlSecurity($input_username);
            $input_password = $this->htmlSecurity($input_password);

            return $this->checkAuthentication($input_username, $input_password, $user_id);
        }else
        {
            return POST_VARIABLES_ERROR;
        }
    }

    /**
     * Vérification des informations de l'utilisateur
     */
    public function checkAuthentication($input_username, $input_password, &$user_id)
    {
        $input_username = $this->htmlSecurity($input_username);
        $input_password = $this->htmlSecurity($input_password);

        $input_password = hash('sha256', $input_password);

        $query = $this->$connection->prepare('SELECT user_id, username, password FROM users WHERE username = ? and password = ?');
        $query->bind_param('ss', $input_username, $input_password);

        $query->execute();

        $result = $query->get_result();
        
        $state_query = ($result->num_rows == 1) ? AUTHENTICATION_SUCCESS : AUTHENTICATION_FAILED;
       
        $user_id = $result->fetch_assoc()['user_id'];
        $result->close();
       
        return $state_query;
    }

    /**
     * Changement du mot de passe
     */
    public function updatePwd($user_id, $new_pwd)
    {
        $user_id = $this->htmlSecurity($user_id);
        $new_pwd = $this->htmlSecurity($new_pwd);

        $new_pwd = hash('sha256', $new_pwd);

        $query = $this->$connection->prepare('UPDATE users SET password = ? WHERE user_id = ?');
        $query->bind_param('sd', $new_pwd, $user_id);

        if ($query->execute())
        {
            return CHANGE_PWD_SUCCESS;
        }

        return CONNECTION_MYSQL_ERROR;
    }

    /**
     * Recuperation du nom d'utilisateur avec son identifiant utilisateur
     */
    public function getUsernameByUserID($my_user_id)
    {
        $my_user_id = $this->htmlSecurity($my_user_id);

        $query = $this->$connection->prepare('SELECT username FROM users WHERE user_id = ?');
        $query->bind_param('d', $my_user_id);

        $query->execute();

        $result = $query->get_result();

        if ($result->num_rows != 1)
        {
            $result->close();
            return -1;
        }

        $username = $result->fetch_assoc()['username'];
        $result->close();
        return $username;
    }

    /**
     * Recuperation du grade de l'utilisateur (Etudiant ou Professeur)
     */
    public function getPermissionsGradeByUserID($my_user_id)
    {
        $my_user_id = $this->htmlSecurity($my_user_id);

        $query = $this->$connection->prepare('SELECT permissions_grade FROM users WHERE user_id = ?');
        $query->bind_param('d', $my_user_id);

        $query->execute();

        $result = $query->get_result();

        if ($result->num_rows != 1)
        {
            $result->close();
            return -1;
        }

        $permissions_grade = $result->fetch_assoc()['permissions_grade'];
        $result->close();
        return $permissions_grade;
    }

    /**
     * Recuperation des classes à laquelle l'utilisateur participe
     */
    public function getClassNameByUserID($my_user_id)
    {
        $my_user_id = $this->htmlSecurity($my_user_id);

        $query = $this->$connection->prepare('SELECT DISTINCT class_name FROM users_class_lessons NATURAL JOIN users NATURAL JOIN lessons NATURAL JOIN class WHERE user_id = ?');
        $query->bind_param('d', $my_user_id);

        $query->execute();

        $result = $query->get_result();

        if ($result->num_rows < 1)
        {
            $result->close();
            return array();
        }

        $array_class_name;

        for ($i = 0; $i < $result->num_rows; $i++)
        {
            $array_class_name[$i] = $result->fetch_assoc()['class_name'];
        }

        $result->close();
        return $array_class_name;
    }

    /**
     * Recuperation de l'identifiant de la classe "class_name"
     */
    public function getClassIDByClassName($class_name)
    {
        $class_name = $this->htmlSecurity($class_name);

        $query = $this->$connection->prepare('SELECT class_id FROM class WHERE class_name = ?');
        $query->bind_param('s', $class_name);

        $query->execute();

        $result = $query->get_result();

        if ($result->num_rows != 1)
        {
            $result->close();
            return -1;
        }

        $class_id = $result->fetch_assoc()['class_id'];
        $result->close();
        return $class_id;
    }

    /**
     * Recuperation du nom de la classe avec son identifiant
     */
    public function getClassNameByClassID($class_id)
    {
        $class_id = $this->htmlSecurity($class_id);

        $query = $this->$connection->prepare('SELECT class_name FROM class WHERE class_id = ?');
        $query->bind_param('d', $class_id);

        $query->execute();

        $result = $query->get_result();

        if ($result->num_rows != 1)
        {
            $result->close();
            return -1;
        }

        $class_name = $result->fetch_assoc()['class_name'];
        $result->close();
        return $class_name;
    }

    /**
     * Recuperations des cours participés par l'utilisateur (my_user_id)
     */
    public function getLessonsByUserID($my_user_id)
    {
        ManagerSession::start();

        $path_lessons = $_SESSION['path_lessons'];

        $current_class_name = LoadingHtmlLessons::getClassFromPathFtp($path_lessons);

        $current_class_id = $this->getClassIDByClassName($current_class_name);

        if (empty($current_class_name))
        {
            return -1;
        }

        $my_user_id = $this->htmlSecurity($my_user_id);
        $current_class_id = $this->htmlSecurity($current_class_id);

        $query = $this->$connection->prepare('SELECT DISTINCT lesson_name FROM users_class_lessons NATURAL JOIN users NATURAL JOIN lessons NATURAL JOIN class WHERE user_id = ? And class_id = ?');
        $query->bind_param('dd', $my_user_id, $current_class_id);

        $query->execute();

        $result = $query->get_result();

        $lessons = array();
        $index_lesson = 0;

        while ($row = $result->fetch_assoc())
        {
            $lessons[$index_lesson] = $row['lesson_name'];
            $index_lesson++;
        }

        $result->close();
        return $lessons;
    }

    /**
     * Récuperation de toutes les leçons
     */
    public function getAllLessons()
    {
        $query = $this->$connection->prepare('SELECT * FROM lessons');

        $query->execute();

        $result = $query->get_result();

        $lessons = array();
        $index_lesson = 0;

        while ($row = $result->fetch_assoc())
        {
            $lessons[$index_lesson] = $row['lesson_name'];
            $index_lesson++;
        }

        $result->close();
        return $lessons;
    }

    /**
     * Récuperation de toutes les leçons
     */
    public function getAllClass()
    {
        $query = $this->$connection->prepare('SELECT * FROM class');

        $query->execute();

        $result = $query->get_result();

        $class = array();
        $index_class = 0;

        while ($row = $result->fetch_assoc())
        {
            $class[$index_class] = $row['class_name'];
            $index_class++;
        }

        $result->close();
        return $class;
    }

    /**
     * Récuperation du nom du cours avec son identifiant ($lesson_id)
     */
    public function getLessonNameByLessonID($lesson_id)
    {
        $lesson_id = $this->htmlSecurity($lesson_id);

        $query = $this->$connection->prepare('SELECT lesson_name FROM lessons WHERE lesson_id = ?');
        $query->bind_param('d', $lesson_id);

        $query->execute();

        $result = $query->get_result();

        if ($result->num_rows != 1)
        {
            return -1;
        }

        $lesson_name = $result->fetch_assoc()['lesson_name'];
        $result->close();
        return $lesson_name;
    }

    /**
     * Récuperations des classes et des cours auquel l'utilisateur participe
     */
    public function getListClassAndLessons($user_id)
    {
        $user_id = $this->htmlSecurity($user_id);

        $query = $this->$connection->prepare('SELECT class_id, lesson_id FROM users_class_lessons WHERE user_id = ?');
        $query->bind_param('d', $user_id);

        $query->execute();

        $result = $query->get_result();

        $list_class_lessons = array();

        $index_class_lessons = 0;

        while ($row = $result->fetch_assoc())
        {
            $class_lessons = new ClassAndLessons();
            $class_lessons->class_id = $row['class_id'];
            $class_lessons->lesson_id = $row['lesson_id'];

            $list_class_lessons[$index_class_lessons] = $class_lessons;
            $index_class_lessons++;
        }

        $result->close();
        return $list_class_lessons;
    }

    /**
     * Recuperations des devoirs de l'utilisateur (user_id)
     */
    public function getHomeworks($user_id)
    {
        $user_id = $this->htmlSecurity($user_id);

        date_default_timezone_set('Europe/Paris');
        $current_date = $this->htmlSecurity(date('Y-m-d', time()));

        $query = $this->$connection->prepare('SELECT homework_id, homework_name, dead_line, messages, class_id, lesson_id, lesson_name FROM class_lessons_homeworks natural join homeworks natural join lessons where dead_line > ? and class_id = SOME(select class_id from users_class_lessons where user_id = ?) and lesson_id = SOME(SELECT lesson_id from users_class_lessons where user_id = ?)');
        $query->bind_param('sdd', $current_date, $user_id, $user_id);

        $query->execute();

        $result = $query->get_result();

        $list_homeworks = array();
        $index_homework = 0;

        while ($row = $result->fetch_assoc())
        {
            $homework_item = new HomeworksStruct();
            $homework_item->id = $row['homework_id'];
            $homework_item->name = $row['homework_name'];
            $homework_item->dead_line = $row['dead_line'];

            $msg_length = strlen($row['messages']);
            $homework_item->messages = "";
            
            for ($i = 0; $i < $msg_length; $i++)
            {
                if ($row['messages'][$i] == '\\' && strlen($row['messages']) + 1 > $i)
                {
                        if ($row['messages'][$i + 1] == 'n') // Saut de ligne
                        {  
                            $i += 1;
                            $homework_item->messages .= "<br/>";
                        }else
                        {
                            $homework_item->messages .= $row['messages'][$i];
                        }
                }else
                {
                    $homework_item->messages .= $row['messages'][$i];
                }
            }

            $homework_item->class_id = $row['class_id'];
            $homework_item->lesson_id = $row['lesson_id'];
            $homework_item->lesson_name = $row['lesson_name'];

            $codage = $homework_item->class_id . "_" . $homework_item->lesson_id . "_" . $homework_item->id;
            $homework_item->codage = $codage;
            
            $list_homeworks[$index_homework] = $homework_item;
            $index_homework++;
        }

        $result->close();
        return $list_homeworks;
    }

    /**
     * Récuepration des devoirs en fonction de l'identifiant du professeur (Pour manager les devoirs)
     */
    public function getHomeworksForManager($user_id)
    {
        $user_id = $this->htmlSecurity($user_id);

        $query = $this->$connection->prepare('SELECT * FROM homeworks WHERE user_id = ?');
        $query->bind_param('d', $user_id);

        $query->execute();

        $result = $query->get_result();

        $list_homeworks = array();
        $index_homework = 0;

        while ($row = $result->fetch_assoc())
        {
            $homework_item = new HomeworksStruct();
            $homework_item->id = $row['homework_id'];
            $homework_item->name = $row['homework_name'];
            $homework_item->dead_line = $row['dead_line'];

            $list_homeworks[$index_homework] = $homework_item;
            $index_homework++;
        }

        $result->close();
        return $list_homeworks;
    }

    /**
     * Vérification si le devoir concerne bien l'utilisateur
     */
    public function checkCodageHomeworks($codage, $user_id)
    {
        if (!LoadingHtmlHomeworks::checkCodage($codage))
        {
            return false;
        }
        
        $codage = LoadingHtmlHomeworks::getCodageArray($codage);

        $user_id = $this->htmlSecurity($user_id);

        $class_id = $this->htmlSecurity($codage[0]);
        $lesson_id = $this->htmlSecurity($codage[1]);
        $homework_id = $this->htmlSecurity($codage[2]);

        date_default_timezone_set('Europe/Paris');
        $current_date = $this->htmlSecurity(date('Y-m-d', time()));

        $query = $this->$connection->prepare('SELECT * FROM users_class_lessons ucl WHERE user_id = ? and class_id = ? and lesson_id = ? and EXISTS(SELECT * FROM class_lessons_homeworks clh natural join homeworks h WHERE clh.class_id = ucl.class_id and clh.lesson_id = ucl.lesson_id and homework_id = ? and h.dead_line > ?)');
        $query->bind_param('dddds', $user_id, $class_id, $lesson_id, $homework_id, $current_date);

        $query->execute();

        $result = $query->get_result();

        if ($result->num_rows == 1)
        {
            $result->close();
            return true;
        }

        $result->close();
        return false;
    }

    /**
     * Ajout d'un devoir
     */
    public function addHomework($class_id, $lesson_id, $homework_name, $dead_line, $additionnals_infos)
    {
        $class_id = $this->htmlSecurity($class_id);
        $lesson_id = $this->htmlSecurity($lesson_id);
        $homework_name = $this->htmlSecurity($homework_name);
        $dead_line = date($this->htmlSecurity($dead_line));
        $additionnals_infos = $this->htmlSecurity($additionnals_infos);

        $user_id = $this->htmlSecurity($_SESSION['user_id']);

        // Ajout du devoir
        $query = $this->$connection->prepare("INSERT INTO homeworks (user_id, homework_name, dead_line, messages) VALUES (?, ?, ?, ?)");
        $query->bind_param('dsss', $user_id, $homework_name, $dead_line, $additionnals_infos);

        $query->execute();

        $homework_id = mysqli_insert_id($this->$connection);

        // Ajout du devoir aux concernés ($class_id, $lesson_id)

        $query = $this->$connection->prepare("INSERT INTO class_lessons_homeworks (class_id, lesson_id, homework_id) VALUES (?, ?, ?)");
        $query->bind_param('ddd', $class_id, $lesson_id, $homework_id);

        $query->execute();
    }

    /**
     * Vérifie si le devoir 'homework_id' a bien été créé par le 'user_id' (Un enseignant en l'occurence)
     */
    public function checkHomeworkIDTeacher($homework_id, $user_id)
    {
        $homework_id = $this->htmlSecurity($homework_id);
        $user_id = $this->htmlSecurity($user_id);

        $query = $this->$connection->prepare('SELECT * FROM homeworks WHERE homework_id = ? and user_id = ?');
        $query->bind_param('dd', $homework_id, $user_id);

        $query->execute();

        $result = $query->get_result();

        if ($result->num_rows == 1)
        {
            $result->close();
            return true;
        }

        $result->close();
        return false;
    }

    /**
     * Récuperation du codage d'un devoir (forme : X_Y_Z)
     */
    public function getCodageByHomeworkID($homework_id)
    {
        $homework_id = $this->htmlSecurity($homework_id);

        $query = $this->$connection->prepare('SELECT class_id, lesson_id FROM class_lessons_homeworks WHERE homework_id = ?');
        $query->bind_param('d', $homework_id);

        $query->execute();

        $result = $query->get_result();
		
        if ($result->num_rows == 1)
        {
            $rows = $result->fetch_assoc();
            $codage = $rows['class_id'] . "_" . $rows['lesson_id'] . "_" . $homework_id;
            $result->close();
            return $codage;
        }

        $result->close();
        return "";
    }

    /**
     * Supprime un devoir
     */
    public function deleteHomework($homework_id)
    {
        $homework_id = $this->htmlSecurity($homework_id);

        $query = $this->$connection->prepare('DELETE FROM class_lessons_homeworks WHERE homework_id = ?');
        $query->bind_param('d', $homework_id);

        $query->execute();

        $query = $this->$connection->prepare('DELETE FROM homeworks WHERE homework_id = ?');
        $query->bind_param('d', $homework_id);

        $query->execute();
    }

    
    /**
     * Ajout d'un compte utilisateur
     */
    public function addUser($username, $password, $grade)
    {
        if (!$this->checkUsername($username) || !$this->checkPassword($password))
        {
            return ADD_USER_ERROR;
        }

        $username = $this->htmlSecurity($username);
        $password = $this->htmlSecurity($password);
        $grade = $this->htmlSecurity($grade);

        $password = hash('sha256', $password);

        $query = $this->$connection->prepare('SELECT * FROM users WHERE username = ?');
        $query->bind_param('s', $username);

        if (!$query->execute())
            return ADD_USER_ERROR;

        $result = $query->get_result();

        if ($result->num_rows > 0)
        {
            $result->close();
            return USERNAME_INPUT_EXISTS;
        }

        $result->close();

        $query = $this->$connection->prepare('INSERT INTO users (username, password, permissions_grade) VALUES (?, ?, ?)');
        $query->bind_param('ssd', $username, $password, $grade);

        if (!$query->execute())
            return ADD_USER_ERROR;

        return ADD_USER_SUCCESS;
    }

    /**
     * Supprime un compte utilisateur
     */
    public function removeUser($username)
    {
        $username = $this->htmlSecurity($username);

        $query = $this->$connection->prepare('SELECT * FROM users WHERE username = ?');
        $query->bind_param('s', $username);

        $query->execute();

        $result = $query->get_result();

        if ($result->num_rows > 0)
        {
            $query = $this->$connection->prepare('SELECT * FROM users WHERE username = ? And (permissions_grade = 0 OR permissions_grade = 1)');
            $query->bind_param('s', $username);

            $query->execute();
            $result = $query->get_result();

            if ($result->num_rows > 0)
            {
                $query = $this->$connection->prepare('DELETE FROM users WHERE username = ?');
                $query->bind_param('s', $username);
    
                $query->execute();
    
                $result->close();
                return REMOVE_USER_SUCCESS;
            }

            $result->close();
            return GRADE_USER_ERROR;
        }else
        {
            $result->close();
            return USERNAME_NOT_EXISTS;
        }
    }

    /**
     * Ajoute une classe
     */
    public function addClass($class_name)
    {
        $class_name = $this->htmlSecurity($class_name);

        $query = $this->$connection->prepare('INSERT INTO class (class_name) VALUES (?)');
        $query->bind_param('s', $class_name);

        if ($query->execute())
           return ADD_CLASS_SUCCESS;
        else
           return CLASS_NAME_EXISTS;
    }

    /**
     * Supprime une classe
     */
    public function deleteClass($class_name)
    {
        $class_name = $this->htmlSecurity($class_name);

        $query = $this->$connection->prepare('DELETE FROM class WHERE class_name = ?');
        $query->bind_param('s', $class_name);

        $query->execute();
    }

    /**
     * Ajoute une leçon
     */
    public function addLesson($lesson_name)
    {
        $lesson_name = $this->htmlSecurity($lesson_name);

        $query = $this->$connection->prepare('INSERT INTO lessons (lesson_name) VALUES (?)');
        $query->bind_param('s', $lesson_name);

        if ($query->execute())
           return ADD_LESSON_SUCCESS;
        else
           return LESSON_NAME_EXISTS;
    }

    /**
     * Supprime une leçon
     */
    public function deleteLesson($lesson_name)
    {
        $lesson_name = $this->htmlSecurity($lesson_name);

        $query = $this->$connection->prepare('DELETE FROM lessons WHERE lesson_name = ?');
        $query->bind_param('s', $lesson_name);

        $query->execute();
    }

    /**
     * Récupère l'identifiant, le nom ainsi que le grade de tous les utilisateurs
     */
    public function get_users_data()
    {
        $query = $this->$connection->prepare('SELECT user_id, username, permissions_grade FROM users ORDER BY permissions_grade DESC');

        $query->execute();

        $result = $query->get_result();

        $array_users_data = array();
        $index_array = 0;

        while ($row = $result->fetch_assoc())
        {
            $array_users_data[$index_array++] = $row;
        }
        
        $result->close();
        return $array_users_data ;
    }

    /**
     * Récupère toutes les classes (identifiant et nom)
     */
    public function get_class()
    {
        $query = $this->$connection->prepare('SELECT * FROM class');

        $query->execute();

        $result = $query->get_result();

        $array_class = array();
        $index_array = 0;

        while ($row = $result->fetch_assoc())
        {
            $array_class[$index_array++] = $row;
        }
        
        $result->close();
        return $array_class;
    }

    /**
     * Récupère toutes les matières (identifiant et nom)
     */
    public function get_lessons()
    {
        $query = $this->$connection->prepare('SELECT * FROM lessons');

        $query->execute();

        $result = $query->get_result();

        $array_lessons = array();
        $index_array = 0;

        while ($row = $result->fetch_assoc())
        {
            $array_lessons[$index_array++] = $row;
        }
        
        $result->close();
        return $array_lessons;
    }

    /**
     * Ajoute une relation entre un utilisateur, une classe et une matière
     */
    public function add_user_class_lesson($user_id, $class_id, $lesson_id)
    {
        $user_id = $this->htmlSecurity($user_id);
        $class_id = $this->htmlSecurity($class_id);
        $lesson_id = $this->htmlSecurity($lesson_id);

        $query = $this->$connection->prepare('INSERT INTO users_class_lessons (user_id, class_id, lesson_id) VALUES (?, ?, ?)');
        $query->bind_param('ddd', $user_id, $class_id, $lesson_id);
        
        if ($query->execute())
           return ADD_USER_CLASS_LESSON_SUCCESS;
        else
           return ADD_USER_CLASS_LESSON_FAILED;
    }

    /**
     * Modification d'une variable afin de la rendre + sécurisée
     */
    public function htmlSecurity($variable)
    {
        return htmlspecialchars($variable, ENT_IGNORE, 'utf-8');
    }
}

/**
 * Structure d'un devoir
 */
class HomeworksStruct
{
    public $id;
    public $name;
    public $dead_line;
    public $messages;
    public $class_id;
    public $lesson_id;
    public $lesson_name;
    public $codage;
    public $user_id;
}

/**
 * Structure des cours reliés aux classes
 */
class ClassAndLessons
{
    public $class_id;
    public $lesson_id;
}
?>		