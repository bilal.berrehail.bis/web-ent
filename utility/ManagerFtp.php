<?php

include_once('Header.php');

/* A CHANGER */
define('SERVER_FTP', 'ftp.cluster021.hosting.ovh.net');

define('USERNAME_STUDENT_FTP', 'devbrlcobk');
define('PASSWORD_STUDENT_FTP', 'ffWQuQvFM69kf');

define('USERNAME_TEACHER_FTP', 'devbrlcobk');
define('PASSWORD_TEACHER_FTP', 'ffWQuQvFM69kf');

/* STATE CODE FOR PATH_DELETE */
define('PATH_DELETE_SUCCESS', 10);
define('PATH_DELETE_FAILED', 11);

/* STATE CODE FOR CREATE_FOLDER */
define('CREATE_FOLDER_SUCCESS', 12);
define('CREATE_FOLDER_FAILED', 13);

/* STATE CODE FOR UPLOAD_FILE */
define('UPLOAD_FILE_SUCCESS', 14);
define('UPLOAD_FILE_FAILED', 15);
define('UPLOAD_FILE_ALREADY_EXISTS', 16);
define('UPLOAD_FILE_TOO_BIG', 17);
define('UPLOAD_FILES_LESSON_SIZE_DIRECTORY_TOO_BIG', 18);

/* SIZE MAX FOR A FILE */
define('FILE_SIZE_MAX_MO', 50);

/* SIZE MAX FOR FILES IN A LESSON */
define('FILES_SIZE_MAX_LESSON_MO', 10);

/* PATH FOR CLASS */
define('START_PATH', '/Cours');

/* PATH FOR THE HOMEWORKS */
define('PATH_HOMEWORKS', '/Devoirs');

/* SIZE MAX FOR A HOMEWORK */
define('FILE_SIZE_MAX_MO_HW', 50);

class ManagerFtp
{
    private $connectionID;

    /**
     * Constructeur de la classe
     */
    function __construct($isteacher)
    {
        if ($isteacher)
            $this->connectionAsTeacher();
        else
            $this->connectionAsStudent();
    }

    /**
     * Récupère l'unique instance de la classe
     */
    public static function getInstance($isteacher)
    {
        return new ManagerFtp($isteacher);
    }
    
    /**
     * Effectue une connexion au serveur FTP avec les identifiants ayant les droits d'un étudiant
     */
    public function connectionAsStudent()
    {
        $this->$connectionID = ftp_connect(SERVER_FTP);
        $login_result = ftp_login($this->$connectionID, USERNAME_STUDENT_FTP, PASSWORD_STUDENT_FTP);
        ftp_pasv($this->$connectionID, true);
    }

    /**
     * Effectue une connexion au serveur FTP avec les identifiants ayant les droits d'un professeur
     */
    public function connectionAsTeacher()
    {
        $this->$connectionID = ftp_connect(SERVER_FTP);
        $login_result = ftp_login($this->$connectionID, USERNAME_TEACHER_FTP, PASSWORD_TEACHER_FTP);
        ftp_pasv($this->$connectionID, true);
    }

    /**
     * Récuperation d'une liste contenant tous les élements d'un dossier
     */
    public function getListFilesInFolder($path_name)
    {
        $nlist = ftp_nlist($this->$connectionID, $path_name);

        if (!$nlist)
        {
            return array();
        }

        $nlist = array_values(array_filter($nlist, function ($value) { return $value != "." && $value != ".."; }));

        $nlist = array_map(function ($value) use ($path_name) { return $path_name . "/" . $value; }, $nlist);

        return $nlist;
    }

    /**
     * Création d'un dossier de cours
     */
    public function createDirectoryClass($class_name)
    {
        $path_class = START_PATH . '/' . $class_name;

        if (!ftp_mkdir($this->$connectionID, $path_class))
        {
            ftp_rmdir($this->$connectionID, $path_class);
            return ADD_CLASS_FTP_ERROR;
        }

        if (!$this->fillDirectoryClassWithLessons($class_name))
        {
            ftp_rmdir($this->$connectionID, $path_class);
            return FILL_CLASS_FTP_ERROR;
        }

        return ADD_CLASS_FTP_SUCCESS;
    }

    /**
     * Remplir le dossier de classe '$class_name' avec toutes les lessons
     */
    public function fillDirectoryClassWithLessons($class_name)
    {
        $path_class = START_PATH . '/' . $class_name;

        $lessons = $_SESSION['instanceMysql']->getAllLessons();

        foreach ($lessons as $lesson)
        {
            if (!ftp_mkdir($this->$connectionID, $path_class . '/' . $lesson))
            {
                return false;
            }
        }

        return true;
    }

    /**
     * Ajout d'un dossier de lesson '$lesson_name' dans tous les dossiers de classe
     */
    public function fillAllDirectoryClassWithLesson($lesson_name)
    {
        $class = $_SESSION['instanceMysql']->getAllClass();

        foreach ($class as $o_class)
        {
            $path_class_lesson = START_PATH . '/' . $o_class . '/' . $lesson_name;
             
            if (!ftp_mkdir($this->$connectionID, $path_class_lesson))
            {
                return ADD_LESSON_FTP_ERROR;
            }
        }

        return ADD_LESSON_FTP_SUCCESS;
    }

    /**
     * Suppression des dossiers correspondant à la matière '$lesson_name' de tous les dossiers de classes
     */
    public function deleteAllDirectoryLesson($lesson_name)
    {
        $class = $_SESSION['instanceMysql']->getAllClass();

        foreach ($class as $o_class)
        {
            $path_class_lesson = START_PATH . '/' . $o_class . '/' . $lesson_name;

            ftp_rmdir($this->$connectionID, $path_class_lesson);
        }
    }

    /**
     * Suppression d'un dossier ainsi que tous les élements à l'intérieur
     */
    public function removeDirectoryRecursive($path)
    {
        $connID = $this->$connectionID;
		
        if (ftp_rmdir($connID, $path))
        {
            return true;
        }

        if(!(ftp_rmdir($connID, $path) || ftp_delete($connID, $path)))
        {
            $list_files = $this->getListFilesInFolder($path);
			
            foreach ($list_files as $file)
            {
                if (!ftp_rmdir($connID, $file))
                {
                    $this->removeDirectoryRecursive($file);
                }
            }
    
            return ftp_rmdir($connID, $path);
        }
    }

    /**
     * Suppression d'un fichier ou d'un dossier (Pour les professeurs)
     */
    public function deleteByPath($path)
    {
        if (LoadingHtmlLessons::getCountDirectory($path) >= DIRECTORY_COUNT_FOR_LESSONS + 2)
        {
            if (LoadingHtmlLessons::classIsCorrectForUser($path))
            {
                if (LoadingHtmlLessons::lessonIsCorrectForUser($path))
                {
                    $state_path_delete = ($this->isFolder($path, true) ? $this->removeDirectoryRecursive($path) : ftp_delete($this->$connectionID, $path));
                    if ($state_path_delete)
                    {
                        return PATH_DELETE_SUCCESS;
                    }
                }
            }
        }

       return PATH_DELETE_FAILED;
    }

    /**
     * Création d'un dossier
     */
    public function createFolder($path)
    {
        if (LoadingHtmlLessons::getCountDirectory($path) >= DIRECTORY_COUNT_FOR_LESSONS + 2)
        {
            if (LoadingHtmlLessons::classIsCorrectForUser($path))
            {
                if (LoadingHtmlLessons::lessonIsCorrectForUser($path))
                {
                    if ($this->checkFolderName(basename($path)))
                    {
                        $state_create_folder = ftp_mkdir($this->$connectionID, $path);
                    
                        if (strpos($state_create_folder, $_SESSION['path_lessons']) !== false)
                        {
                            return CREATE_FOLDER_SUCCESS;
                        }
                    }
                }
            }
        }

        return CREATE_FOLDER_FAILED;
    }

    /**
     * Vérifie si le nom du dossier respecte certaines normes
     */
    public function checkFolderName($folder_name)
    {
        return !strpos($folder_name, '\'') && !strpos($folder_name, '\"');
    }

    /**
     * Ajout de fichiers au chemin spécifié en premier paramètre "files_path_name" (Uniquement pour les professeurs)
     */
    public function uploadFileOnServerSide($files_name, $files_size, $files_path_tmp_name)
    {
        /**
         * Remplace le caractère (') par un espace, puisqu'il empêche l'éxecution des commandes javascript:postMethod (de LoadingHtmlLessons.php) (Il y en a 3)
         */
        function escapeSimpleQuote($elem)
        {
            return str_replace('\'', ' ', $elem);
        }
        $files_name = array_map("escapeSimpleQuote", $files_name);

        $passed= false;

        $current_path_lessons = $_SESSION['path_lessons'];

        if (LoadingHtmlLessons::getCountDirectory($current_path_lessons) >= DIRECTORY_COUNT_FOR_LESSONS + 1) // + 1 car on veut le repértoire courant et non agir sur un fichier/dossier à l'intérieur de celui-ci
        {
            if (LoadingHtmlLessons::classIsCorrectForUser($current_path_lessons))
            {
                if (LoadingHtmlLessons::lessonIsCorrectForUser($current_path_lessons))
                {
                    $passed= true;
                }
            }
        }

        if (!$passed)
            return;

        $count_upload_success = 0;

        $target_dir = "tmp_uploads_teachers/" . $_SESSION['user_id'] . "/";

        if (!file_exists($target_dir))
        {
            mkdir($target_dir);
        }

        $upload_code = UPLOAD_FILE_FAILED;

        for ($i = 0; $i < count($files_name); $i++)
        {
            $upload_code = UPLOAD_FILE_FAILED;
            $path_file_local = $target_dir . $files_name[$i];

            if ($this->getFileSizeByDirectory(LoadingHtmlLessons::getClassPathFromPathFtp($current_path_lessons)) + $files_size[$i] > FILES_SIZE_MAX_LESSON_MO * 1000000)
            {
                $upload_code = UPLOAD_FILES_LESSON_SIZE_DIRECTORY_TOO_BIG;
            }

            if ($files_size[$i] > FILE_SIZE_MAX_MO * 1000000)
            {
                $upload_code = UPLOAD_FILE_TOO_BIG;
            }
            
            if ($upload_code == UPLOAD_FILE_FAILED)
            {
                if (move_uploaded_file($files_path_tmp_name[$i], $path_file_local))
                {
                    $upload_code = $this->uploadFile($path_file_local, $current_path_lessons);

                    unlink($path_file_local);
                    
                    if ($upload_code == UPLOAD_FILE_SUCCESS) // On met succès si il y a au moins eu 1 upload reussit
                        $count_upload_success++;
                }
            }
        }
        
        if ($count_upload_success > 0)
            $upload_code = UPLOAD_FILE_SUCCESS;
        
        return $upload_code;
    }

    /**
     * Ajout d'un fichier au chemin spécifié en paramètre "path_file_local" (Partie 2 de la fonction uploadFileOnServeurSide(X, X, X) (Uniquement pour les professeurs)
     */
    public function uploadFile($path_file_local, $current_path_lessons)
    {
        $fd = fopen($path_file_local, 'r');   
        $file_name = basename($path_file_local); 

        $path_file_ftp = $current_path_lessons . "/" . $file_name;
        
        $fileSize = ftp_size($this->$connectionID, $path_file_ftp);

        if ($fileSize != -1)
        {
            return UPLOAD_FILE_ALREADY_EXISTS;
        }
         
        if (ftp_fput($this->$connectionID, $path_file_ftp, $fd, FTP_BINARY))
        {
            return UPLOAD_FILE_SUCCESS;
        }

        return UPLOAD_FILE_FAILED;
    }

    /**
     * Récuperation de la taille d'un fichier ou d'un dossier (avec les élements compris à l'intérieur d'un dossier)
     */
    public function getFileSizeByDirectory($directory_ftp)
    {
        $connID = $this->$connectionID;

        $file_size_all = 0;

        if ($this->isFolder($directory_ftp))
        {
            $list_files_ftp = $this->getListFilesInFolder($directory_ftp);
            
            foreach ($list_files_ftp as $file_path)
            {
                if ($this->isFolder($file_path))
                {
                    $file_size_all += $this->getFileSizeByDirectory($file_path);
                }else
                {
                    $file_size_all += ftp_size($connID, $file_path);
                }
            }
        }else
        {
            return ftp_size($connID, $directory_ftp);
        }

        return $file_size_all;
    }

    /**
     * Renvoi si l'élement spécifié en premier paramètre "path" est un dossier ou non
     */
    public function isFolder($path, $is_teacher = false)
    {
        return is_dir($this->getUrlByPath($path, $is_teacher));
    }

    /**
     * Création d'un URL permettant d'accéder au chemin spécifié en premier paramètre "path"
     */
    public function getUrlByPath($path, $is_teacher = false)
    {
        return ($is_teacher ? "ftp://" . USERNAME_TEACHER_FTP . ":" . PASSWORD_TEACHER_FTP . "@" . SERVER_FTP . $path : "ftp://" . USERNAME_STUDENT_FTP . ":" . PASSWORD_STUDENT_FTP . "@" . SERVER_FTP . $path);
    }

    /**
     * Lancement du téléchargement d'un fichier
     */
    public function downloadFile($file_path, $is_teacher)
    {
        $ftp_url = $this->getUrlByPath($file_path, $is_teacher);

        $filename = basename($ftp_url);

        if (strcmp($filename, "ftp.cluster021.hosting.ovh.net") != 0) // TODO : A revoir ?
        {
            header('Content-Type: application/download');
            header('Content-Disposition: attachment; filename=' . $filename);
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($ftp_url));
            ob_clean();
            flush();
            readfile($ftp_url);
        }
    }

    // ---- Parie Homeworks ----

    /**
     * Vérification si le dossier spécifié en premier paramètre existe
     */
    public function directoryExists($path)
    {
        return $this->isFolder($path);
    }

    /**
     * Vérification si le devoir a déjà été rendu ou non
     */
    public function homeworkAlreadySent($codage, $user_id)
    {
        $path_homeworks = PATH_HOMEWORKS . "/" . $codage . "/" . $user_id;

        if ($this->directoryExists($path_homeworks))
        {
            $list_files_ftp = $this->getListFilesInFolder($path_homeworks);
            return !empty($list_files_ftp);
        }

        return false;
    }

    /**
     * Recuperation du nom du devoir (codage) rendu par l'utilisateur (user_id)
     */
    public function getFilenameHomeworkSent($codage, $user_id)
    {
        if ($this->homeworkAlreadySent($codage, $user_id))
        {
            $path_homework = PATH_HOMEWORKS . "/" . $codage . "/" . $user_id;
            $list_files_ftp = $this->getListFilesInFolder($path_homework);

            return $list_files_ftp[0]; // Qu'un fichier par devoir
        }
    }

    /**
     * Création d'un dossier pour l'utilisateur (user_id) afin d'y insérer un devoir (codage)
     */
    public function createDirectoryHomeworkForStudent($codage, $user_id)
    {
        $path_homeworks = PATH_HOMEWORKS . "/" . $codage . "/" . $user_id;

        if (!$this->directoryExists($path_homeworks))
        {
            ftp_mkdir($this->$connectionID, $path_homeworks);
        }
    }

    /**
     * Ajout d'un devoir
     */
    public function addHomework($codage, $user_id, $fileHomework_name, $fileHomework_size, $fileHomework_tmp_name)
    {
        if (is_array($fileHomework_name))
        {
            return;
        }

        $target_dir = "tmp_uploads_homeworks/$codage/";

        if (!file_exists($target_dir))
        {
            mkdir($target_dir);
        }

        $target_dir = $target_dir . "$user_id/"; // je répète l'opération car je veux absolument avoir 1 sous dossier dans le dossier codage

        if (!file_exists($target_dir))
        {
            mkdir($target_dir);
        }

        $upload_code = 1;

        $target_file = $target_dir . basename($fileHomework_name);
    
        $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

        if ($fileHomework_size > FILE_SIZE_MAX_MO_HW * 1000000)
        {
            $upload_code = UPLOAD_FILE_TOO_BIG;
        }
            
        if ($upload_code == 1)
        {
            if (move_uploaded_file($fileHomework_tmp_name, $target_file))
            {
                $s_file_path = $target_dir . $fileHomework_name;
                $upload_code = $this->uploadHomework($codage, $user_id, $s_file_path);

                unlink($s_file_path);
            }else
            {
                $upload_code = UPLOAD_FILE_FAILED;
            }
        }

        return $upload_code;
    }

    /**
     * Ajout d'un devoir (Partie 2 de la fonction addHomework(X, X, X, X, X))
     */
    public function uploadHomework($codage, $user_id, $s_file_path)
    {
        $fd = fopen($s_file_path, 'r');   
        $file_name = basename($s_file_path); 

        // ------------ On crée le dossier 'Devoirs' s'il n'existe pas (Il se supprime seul s'il n'y a plus de devoirs en cours)
        $directorySize = ftp_size($this->$connectionID, PATH_HOMEWORKS);
        
        if ($directorySize == -1)
        {
            ftp_mkdir($this->$connectionID, PATH_HOMEWORKS);
        }

        // ------------ On crée un sous répertoire s'il n'existe pas (ici le dossier codage)

        $path_file_ftp = PATH_HOMEWORKS . "/$codage";
        
        $directorySize = ftp_size($this->$connectionID, $path_file_ftp);

        if ($directorySize == -1)
        {
            ftp_mkdir($this->$connectionID, $path_file_ftp);
        }

        // ------------ Et ici le sous répertoire user_id

        $path_file_ftp = $path_file_ftp . "/$user_id";

        $directorySize = ftp_size($this->$connectionID, $path_file_ftp);

        if ($directorySize == -1)
        {
            ftp_mkdir($this->$connectionID, $path_file_ftp);
        }

        // ------------ On vérifie à présent que le fichier que l'on souhaite ajouté n'existe pas déjà (sécurité en trop ?)

        $path_file_ftp = $path_file_ftp . "/" . $file_name;

        $fileSize = ftp_size($this->$connectionID, $path_file_ftp);

        if ($fileSize != -1)
        {
            return UPLOAD_FILE_ALREADY_EXISTS;
        }
        
        $state_upload_file_2 = ftp_fput($this->$connectionID, $path_file_ftp, $fd, FTP_BINARY);
        
        if ($state_upload_file_2)
        {
            return UPLOAD_FILE_SUCCESS;
        }
        
        return UPLOAD_FILE_FAILED;
    }

    /**
     * Télécharge tous les rendus par les élèves concernés du devoir 'homework_id'
     */
    public function downloadRenderingHomework($homework_id)
    {
        $codage = $_SESSION['instanceMysql']->getCodageByHomeworkID($homework_id);
        $path = PATH_HOMEWORKS . "/$codage";
        
        $target_dir = "tmp_rendering";
        $target_dir_2 = $target_dir . "/$codage";
        
        $connID = $this->$connectionID;

        // On créer un dossier contenant tous les rendus du devoir (local)
        if (!file_exists($target_dir))
        {
            mkdir($target_dir);
        }

        if (!file_exists($target_dir_2))
        {
            mkdir($target_dir_2);
        }

        // On ajoute tous les rendus dans un dossier local '$target_dir_2'
        $fd = fopen($target_dir_2 . "/Assignation.ent", "w");
        fwrite($fd, "-- TABLES FAISANT LA RELATION ENTRE LE NOM DES RENDUS ET L'ETUDIANT --");
        fwrite($fd, chr(13) . chr(13));
		
        $count = 0;
		
		if ($this->isFolder($path))
		{
			$list_rendering = $this->getFilesInDirectoryRecursive($path);
		
			foreach ($list_rendering as $rendering)
			{
				$count++;
				$username = $_SESSION['instanceMysql']->getUsernameByUserID($this->getIDByRendering($rendering));

				$local_file = $target_dir_2 . "/" . basename($rendering);

				$num = 1;
				while (file_exists($local_file))
				{
					$local_file = $target_dir_2 . "/" . "($num) " . basename($rendering);
					$num++;
				}

				fwrite($fd, "$username -> $local_file");
				fwrite($fd, chr(13));

				ftp_get($connID, $local_file, $rendering, FTP_BINARY);
			}
		}

        fwrite($fd, chr(13));
        fwrite($fd, "Nombre de devoirs rendus : $count !");
        fclose($fd);

        // On compresse le dossier local '$target_dir_2' afin de le télécharger
        $path_zip = "$target_dir_2/$codage.zip";

        $zip = new ZipArchive;
        if ($zip->open($path_zip, ZipArchive::CREATE) === TRUE)
        {
            if ($dir = opendir($target_dir_2))
            {
                while (false !== ($entry = readdir($dir)))
                {
                    if (strcmp($entry, ".") != 0 && strcmp($entry, ".."))
                    {
                        $zip->addFile($target_dir_2 . "/$entry");
                    }
                }
            }
            $zip->close();
        }

        // Téléchargement de l'archive
        header('Content-Type: application/download');
        header('Content-Disposition: attachment; filename=Rendus.zip');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path_zip));
        ob_clean();
        flush();
        readfile($path_zip);
        // suppression du fichier temporaire
        $this->deleteDirectoryRecursive($target_dir_2);
    }

    /**
     * Supprime un dossier ainsi que son contenu
     */
    public function deleteDirectoryRecursive($directory)
    {
        if (is_dir($directory))
        { 
            $entries = scandir($directory); 
            foreach ($entries as $entry)
            { 
                if (strcmp($entry, ".") != 0 && strcmp($entry, ".."))
                { 
                    if (is_dir($directory . "/" . $entry))
                        rmdir($directory . "/" . $entry);
                    else
                        unlink($directory . "/" . $entry); 
                } 
            }
            rmdir($directory); 
        } 
    }

    /**
     * Recuperation de l'identifiant de l'utilisateur responsable du rendu
     */
    public function getIDByRendering($path_rendering)
    {
        $paths = explode("/", $path_rendering);
        return $paths[count($paths) - 2];
    }

    /**
     * Récuperation de tous les fichiers dans un dossier | TODO A ADAPTER BIEN EVIDEMMENT
     */
    public function getFilesInDirectoryRecursive($directory_ftp)
    {
        $connID = $this->$connectionID;

        $files_array = array();
        $index_array = 0;

        if ($this->isFolder($directory_ftp))
        {
            $list_files_ftp = $this->getListFilesInFolder($directory_ftp);
            
            foreach ($list_files_ftp as $file_path)
            {
                if ($this->isFolder($file_path))
                {
                    $files_array = array_merge($this->getFilesInDirectoryRecursive($file_path), $files_array);
                }else
                {
                    $files_array[$index_array] = $file_path;
                    $index_array++;
                }
            }
        }else
        {
            return $directory_ftp;
        }

        return $files_array;
    }
}

?>			