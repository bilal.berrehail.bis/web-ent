<?php

include_once('utility/Header.php');

define('SERVER_ERROR', '1');
define('AUTHENTICATION_ERROR', '2');

class LoadingHtmlNavbar
{
    /**
     * Chargement de la balise <head>
     */
    public static function loadHeadContent()
    {
?>
        <title>Lycée Baggio - ENT</title>
<?php
        self::loadStylesheet();
        self::loadScript();
    }

    public static function loadStylesheet()
    {
?>
        <link href="css/darkly.css" rel="stylesheet" type="text/css"/>
        
        <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css" rel="stylesheet" type="text/css" />
        <link href="css/glyphicons.css" rel="stylesheet" type="text/css"/> <!-- not used -->
        <link href="css/card.css" rel="stylesheet" type="text/css"/>
<?php
    }

    /**
     * Chargement des scripts de la page
     */
    public static function loadScript()
    {
?>
        <script src="https://bootswatch.com/_vendor/jquery/dist/jquery.min.js"></script>
        <script src="https://bootswatch.com/_vendor/popper.js/dist/umd/popper.min.js"></script>
        <script src="https://bootswatch.com/_vendor/bootstrap/dist/js/bootstrap.min.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>

        <script type="text/javascript" src="js/script.js"></script>
<?php
    }

    /**
     * Chargement de la barre de navigation
     */
    public static function loadNavbar($page_active_index = -1)
    {
        ManagerSession::start();
        $isLogged = ManagerSession::isLogged();

        $menu_options = [
            "Accueil" => "index.php"
        ];

        if ($isLogged)
        {
            $get_permissions_grade = $_SESSION['instanceMysql']->getPermissionsGradeByUserID($_SESSION['user_id']);
            switch ($get_permissions_grade)
            {
                case 0: // Elève simple
                    $menu_options["Mes cours"] = "lessons.php";
                    $menu_options["Devoirs à rendre"] = "homeworks.php";
                    break;
                case 1: // Professeur
                    $menu_options["Administrer mes cours"] = "lessons.php";
                    $menu_options["Ajouter un devoir"] = "homeworksadmin.php";
                    $menu_options["Gestion des devoirs"] = "homeworksmanager.php";
                    break;
                case 2: // Administrateur
                    $menu_options["Invite de commandes"] = "cmd.php";
                    $menu_options["Ajout d'utilisateurs"] = "usersmanager.php";
                    $menu_options["Assignations"] = "assignations.php";
                    break;
            }
        }
?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Baggio ENT</a>
                </div>
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
<?php
                $index = 0;
                $actived = "unknown";

                foreach ($menu_options as $menu_name => $menu_link) {
                    $index++;
                    
                    if ($index == $page_active_index) // On met le focus
                    {
                        echo "<li class=\"active\"><a href=\"$menu_link\">$menu_name<span class=\"sr-only\">(current)</span></a></li>";
                    }
                    else
                    {
                        echo "<li><a href=\"$menu_link\">$menu_name</a></li>";
                    }
                }
                if ($isLogged)
                {
?>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <div style="margin-top:4%;" class="btn-group">
                            <div class="btn-group">
                            <a href="#" class="btn btn-danger dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                Paramètres du compte&nbsp;
                                <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="changepwd.php">Modifier mon mot de passe</a></li>
                                <li><a href="disconnect.php">Se déconnecter</a></li>
                            </ul>
                            </div>
                        </div>
                    </li>
<?php
                }
            echo "</ul>
            </div>
        </nav>";
    }
}
?>