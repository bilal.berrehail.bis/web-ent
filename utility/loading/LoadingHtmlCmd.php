<?php

include_once('utility/Header.php');

class LoadingHtmlCmd
{
    /**
     * Chargement de la page par défaut de l'invite de commandes
     */
    public static function loadingDefaultPage()
    {
?>
        <div class="form-group" style="margin: 3% 20% 0% 20%;">
            <ul id="output" class="list-group" style="border: 1mm ridge rgba(100, 100, 100, .6);">
                <li class="list-group-item">----- Invite de commandes -----</li>
                <li class="list-group-item">Outil d'administration du site de l'ENT (v0.1)</li>
                <li class="list-group-item">Pour plus d'informations sur les commandes disponibles : help !</li>
                <li class="list-group-item">--------------------------------------</li>
            </ul>
            <input id="input" type="text" class="form-control" placeholder="Entrez une commande ...">
        </div>
        <br/>
        <br/>
<?php
    }
}

?>