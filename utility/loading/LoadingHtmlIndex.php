<?php

include_once('utility/Header.php');

class LoadingHtmlIndex
{
    /**
     * Chargement de la page de présentation
     */
    public static function loadIntroductoryText()
    {
?>
        <br/>
                <div style="padding:28px 0 28px 0;" class="jumbotron">
                
                <h1 style="margin:-20px 0 0 2%;font-size:61px;">Bienvenue sur l'ENT du lycée Baggio</h1>

                <hr color="grey" class="my-4">

                <p style="font-size:19px;margin-left:2%;" class="lead">Cet outil vous permettra de visualiser vos cours, de rendre vos TP's ainsi que de récuperer d'éventuels TD's.</p>
<?php    
                ManagerSession::start();
                $isLogged = ManagerSession::isLogged();

                if ($isLogged)
                {
                    echo "<p style=\"font-size:19px;margin-left:2%;\">Accédez aux outils que proposent l'ENT à l'aide de la barre de navigation ci-dessus.</p>";
                    $get_username = $_SESSION['instanceMysql']->getUsernameByUserID($_SESSION['user_id']);

                    $grade_id = $_SESSION['instanceMysql']->getPermissionsGradeByUserID($_SESSION['user_id']);

                    switch ($grade_id)
                    {
                        case 0:
                            $nom_grade = "Elève";
                            break;
                        case 1:
                            $nom_grade = "Enseignant";
                            break;
                        case 2:
                            $nom_grade = "Administrateur";
                            break;
                        default:
                            $nom_grade = "Inconnu";
                            break;
                    }

                    echo "<hr color=\"grey\">
                        <h3 style=\"font-size:19px;margin-left:2%;\" class=\"text-success\">Vous êtes connecté en tant que : <b>$get_username</b> ($nom_grade).</h3>
                        <div style=\"font-size:19px;margin:0 1% 0 1%;\" id=\"tmp_msg\" class=\"alert alert-dismissible alert-success\">   
                            <strong>Parfait!</strong> Vous pouvez dès à présent profiter des fonctionnalitées de l'ENT du lycée Baggio</a>.
                        </div>";
                }else
                {
?>
                    <p style="font-size:19px;margin-left:2%;">Si vous êtes étudiant ou professeur, je vous invite à vous connecter avec vos identifiants fournis par l'administrateur de ce site.</p>
                    <hr color="grey" class="my-4">
                    <div class="form-horizontal">
                        <button style="font-size:25px;margin-left:2%" type="button" class="btn btn-primary" onclick="location.href='login.php'">Se connecter</button>

                    </div>
<?php
                }
?>
        </div>
        
        <blockquote class="blockquote text-center">
            <p class="mb-0">Si vous ne possédez pas encore de compte utilisateur. Renseignez-vous auprès de l'administrateur afin qu'il vous en fournisse un.</p>
            <footer class="blockquote-footer">Cordialement <a href="https://www.dev-brl.com/cv"><cite title="The dev">The dev</cite></footer></a>
        </blockquote>
<?php
    }
}
?>	