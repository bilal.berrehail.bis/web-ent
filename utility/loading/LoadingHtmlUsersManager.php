<?php

include_once('utility/Header.php');

define('PASSWORD_GENERATE_LENGTH', 7);

class LoadingHtmlUsersManager
{
    /**
     * Chargement de la page par défaut
     */
    public static function loadInformations()
    {
?>
    <h3 style="text-align:center;">Ajout rapide de comptes élève</h3>
    <hr color="grey" style="margin: 0 7% 0 7%;">
    <br/>

    <div style="margin:0 30% 0 30%; width:35%" hidden id="tmp_msg" name="state_block">
        <div style="text-align:center;" id="msg_state"></div>
    </div>
    
        
    <div style="height:410px; border-top: solid rgb(100, 100, 100); border-bottom: solid rgb(100, 100, 100);" class="jumbotron">
        <div style="margin-top:-47px;">
        <div style="margin-left:25px;float:left;" class="col-md-3">
            <h4 style="text-align:center;">Liste des élèves</h4>
            <textarea id="infousers" class="form-control" rows="15" placeholder="Ajouter un nom et un prénom séparés par un espace sur chaque ligne." required></textarea>
        </div>

        <div style="float:left;" class="col-md-2">
            <br/><br/><br/><br/>
            <br/><br/><br/>
            <button onclick="createAccountsUser();" style="margin-left:6%;float:left;" type="button" class="btn btn-primary">Créer les comptes >></button>
        </div>
        
        <div style="float:left;" class="col-md-3">
            <h4 style="text-align:center;">Compte(s) crée(s)</h4>
            <textarea id="successaccount" class="form-control" rows="15" placeholder="Les comptes crées seront affichés ici" required></textarea>
        </div>

        <div style="float:left;" class="col-md-3">
            <h4 style="text-align:center;">Erreur(s) rencontrée(s)</h4>
            <textarea id="failedaccount" class="form-control" rows="15" placeholder="Les erreurs seront affichées ici" required></textarea>
        </div>
        </div>

        
    </div>
<?php
    }

    /**
     * Génère des comptes et les ajoute dans la base de données
     */
    public static function createUsers($users)
    {
        $users_length = count($users);

        $data_success = "";
        $data_failed = "";
        
        $count_success = 0;
        foreach ($users as $user)
        {
            $user_splitted = explode(' ', $user);
            $nom = strtolower($user_splitted[0]);
            $prenom = strtolower($user_splitted[1]);

            $letters = array_merge(range('A', 'Z'), range('a', 'z'), range(0, 9));
            $id = 1;

            $password = "";

            for ($i = 0; $i < PASSWORD_GENERATE_LENGTH; $i++)
            {
                $password .= $letters[rand(0, 61)];
            }

            $username = $prenom . '.' . $nom;

            $add_user_state_code = $_SESSION['instanceMysql']->addUser($username, $password, 0);

            while ($add_user_state_code == USERNAME_INPUT_EXISTS)
            {
                $username = $prenom . '.' . $nom . $id++;
                $add_user_state_code = $_SESSION['instanceMysql']->addUser($username, $password, 0);
            }
            if ($add_user_state_code == ADD_USER_SUCCESS)
            {
                $count_success++;
                $data_success .= "data_success ";
                $data_success .= $username . " ";
                $data_success .= " : " . $password;
                $data_success .= "|";
            }
            else if ($add_user_state_code == ADD_USER_ERROR)
            {
                $data_failed .= "data_failed Erreur : ";
                $data_failed .= $user_splitted[0] . " ";
                $data_failed .= $user_splitted[1] . "|";
            }
        }

        echo $data_success;
        echo $data_failed;
        echo "success Nombre de comptes ajoutés : $count_success. Nombre d'échecs : " . ($users_length - $count_success);
    }
}

?>