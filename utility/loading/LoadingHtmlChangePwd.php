<?php

include_once('utility/Header.php');

define('PWD_INCORRECT', 1);
define('PWD_DIFFERENT', 2);
define('PWD_SAME', 3);
define('PWD_BAD', 4);
define('CHANGE_PWD_SUCCESS', 5);
define('ERROR_CAPTCHA', 6);

class LoadingHtmlChangePwd
{
    /**
     * Charge le formulaire de modification du mot de passe
     */
    public static function loadForm($state_code)
    {
?>
        <div class="container">
        <div class="login-form">
            <div style="height:530px;" class="main-div">
                <div style="margin-top:-20px;" class="panel_login">
                    <h2>Modification du mot de passe</h2>
                    <p>Entrez votre ancien mot de passe et votre nouveau mot de passe.</p>
                </div>
                <form id="Login" method="post">
                    <div class="form-group">

                        <input type="password" class="form-control" name="input_old_pwd" placeholder="Ancien mot de passe">

                    </div>

                    <div class="form-group">

                        <input type="password" class="form-control" name="input_pwd" placeholder="Nouveau mot de passe">

                    </div>

                    <div class="form-group">

                        <input type="password" class="form-control" name="input_confirm_pwd" placeholder="Confirmer nouveau mot de passe">
                        
                    </div>
                    <div style="margin-left:-11px;;" class="g-recaptcha" data-sitekey="6Le2FaQUAAAAAKORedBoQTpLMAoqsZMuW5pzxmMe"></div>
                    
                    <button style="margin-top:8px;" type="submit" class="btn btn-primary">Modifier le mot de passe</button>

                </form>
                <?php self::displayStateCode($state_code); ?>
                
            </div>

        </div>
    </div>
<?php
    }

    /**
     * Affichage d'un message d'information en fonction des actions de l'utilisateur
     */
    public static function displayStateCode($state_code)
    {
        switch ($state_code)
        {
            case CONNECTION_MYSQL_ERROR:
                LoadingHtmlLessons::displayMsgTmp("danger", "Serveur inaccessible", "margin: 7px -10% -15% -10%;font-size:13px;");
                break;
            case PWD_INCORRECT:
                LoadingHtmlLessons::displayMsgTmp("danger", "Mot de passe incorrect.", "margin: 7px -10% -15% -10%;font-size:13px;");
                break;
            case PWD_DIFFERENT:
                LoadingHtmlLessons::displayMsgTmp("danger", "Vos mot de passes ne correspondent pas.", "margin: 7px -10% -16% -10%;font-size:13px;");
                break;
            case PWD_SAME:
                LoadingHtmlLessons::displayMsgTmp("danger", "Le nouveau mot de passe ne peut pas être similaire à l'ancien.", "margin: 7px -10% -16% -10%;font-size:13px;");
                break;
            case PWD_BAD:
                LoadingHtmlLessons::displayMsgTmp("danger", "Votre nouveau mot de passe n'est pas conforme.", "margin: 7px -10% -16% -10%;font-size:13px;");
                break;
            case ERROR_CAPTCHA:
                LoadingHtmlLessons::displayMsgTmp("danger", "Confirmez le captcha.", "margin: 7px -10% -16% -10%;font-size:13px;");
                break;
            case CHANGE_PWD_SUCCESS:
                LoadingHtmlLessons::displayMsgTmp("success", "Votre mot de passe a été modifié.", "margin: 7px -10% -16% -10%;font-size:13px;");
                break;
        }
    }
}

?>