<?php

include_once('utility/Header.php');

define('MAX_CHAR_ADDITIONNALS_INFOS', 330);
define('JUMP_LINE_EQUIVALENT_IN_CHAR', 50);
define('MAX_CHAR_HOMEWORK_NAME', 26);

define('WRONG_CLASS_LESSON_INDEX', 1);
define('WRONG_HOMEWORK_NAME', 2);
define('WRONG_DEAD_LINE', 3);
define('WRONG_ADDITIONNALS_INFOS', 4);
define('MYSQL_ERROR', 5);

define('HOMEWORK_ADDED_SUCCESS', 6);

class LoadingHtmlHomeworksAdmin
{
    /**
     * Récupère une liste de structure contenant les classes et les cours de l'user_id
     */
    public static function loadListLessons()
    {
        $list_class_lessons = $_SESSION['instanceMysql']->getListClassAndLessons($_SESSION['user_id']);

        return $list_class_lessons;
    }

    /**
     * Affiche le service de création d'un nouveau devoir
     */
    public static function loadCreateHomeworkService($state_code)
    {
        $list_class_lessons = self::loadListLessons();
?>
        <?php self::displayStateCode($state_code);?>
        <hr color="grey" style="margin: 0 7% 0 7%;">
        <br/>
        <form method="post" id="createHomework">
                <div class="card border-dark mb-3" style="font-family:Calibri; font-size:16px; width:430px; height:495px; margin: 0 auto;">
                    <div class="card-header" style="border-top:solid #375a7f;">
                        <select id=select class="form-control" name="class_lesson_index" style="font-size:17px;float:left;width:240px;">
<?php
                        $index_class_lesson = 0;

                        foreach ($list_class_lessons as $class_lesson)
                        {
                            $class_name = $_SESSION['instanceMysql']->getClassNameByClassID($class_lesson->class_id);
                            $lesson_name = $_SESSION['instanceMysql']->getLessonNameByLessonID($class_lesson->lesson_id);

                            echo "<option value=\"$index_class_lesson\">" . $lesson_name . " (" . $class_name . ")</option>";

                            $index_class_lesson++;
                        }
?>
                        </select>
                        
                        <button type="button" class="btn btn-primary" style="float:right;" onclick="homeworkFormSubmit();">Ajouter ce devoir</button></div>
            
                        <div class="card-body" style="border-right: solid rgb(100, 100, 100); border-bottom: solid rgb(100, 100, 100); border-left: solid rgb(100, 100, 100);">
                        
                        <b><h3 class="card-title" style="margin-top:-3px;text-align:center">Nom du devoir</h3></b>

                        <div class="form-group">
                            <input type="text" name="homework_name" style="font-size:19px;text-align:center;" class="form-control" maxlength="<?php echo MAX_CHAR_HOMEWORK_NAME;?>">
                        </div>

                        <hr color="grey" style="margin: 0 7% 0 7%;">
                            <p class="card-text">
                                
                                    <b style="color:#3498DB">
                                        Date limite :
                                    </b>
                                    <div style="margin:-10px 0 0 27%;" id="datepicker" class="input-group date" data-date-format="yyyy-mm-dd">
                                        <div class="form-group">
                                            <input style="font-size:21px;text-align:center;" class="form-control form-control-lg" type="text" placeholder=".form-control-lg" name="dead_line">
                                        </div>

                                        <span class="input-group-addon"></span>
                                    </div>

                                    <b style="color:#3498DB;">
                                        Informations complémentaires :
                                    </b>

                                    <textarea style="margin-top:8px;" data-limit-rows="true" class="form-control" name="additionnals_infos" maxlength="<?php echo MAX_CHAR_ADDITIONNALS_INFOS;?>" cols="38" rows="8" style="resize:none;scrolling:no;"></textarea>
                                   
                                    <i style="font-size:12px"> <?php echo MAX_CHAR_ADDITIONNALS_INFOS;?> caractères maximum. Saut de ligne = 50 caractères</i>
                                
                        </div>
                    </div>
                </div>
            </form>

            <script>
                function homeworkFormSubmit()
                {
                document.getElementById("createHomework").submit();
                }
            </script>
<?php
    }

    /**
     * Traitement des informations pour l'ajout d'un nouveau devoir
     */
    public static function createHomeworkHandle($class_lesson_index, $homework_name, $dead_line, $additionnals_infos)
    {
        // $class_lesson_index
        $list_class_lessons = self::loadListLessons();

        if ($class_lesson_index < 0 || $class_lesson_index >= count($list_class_lessons))
        {
            return WRONG_CLASS_LESSON_INDEX;
        }

        // $homework_name
        if (strlen($homework_name) <= 0 || strlen($homework_name) > MAX_CHAR_HOMEWORK_NAME)
        {
            return WRONG_HOMEWORK_NAME;
        }

        // $dead_line
        date_default_timezone_set('Europe/Paris');
        $current_date = date('Y-m-d', time());

        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/", $dead_line) || $dead_line <= $current_date)
        {
            return WRONG_DEAD_LINE;
        }

        // additionnals_infos
        $char_count = 0;
        $additionnals_infos_changed = "";
        
        for ($i = 0; $i < strlen($additionnals_infos); $i++)
        {
            if (ord($additionnals_infos[$i]) == 13)
            {
                $additionnals_infos_changed .= "\\n";
                $char_count += JUMP_LINE_EQUIVALENT_IN_CHAR;
            }else
            {
                $additionnals_infos_changed .= $additionnals_infos[$i];
                $char_count++;
            }
        }

        if ($char_count > MAX_CHAR_ADDITIONNALS_INFOS)
        {
            return WRONG_ADDITIONNALS_INFOS;
        }

        // Ajout du devoir
        if ($_SESSION['instanceMysql']->addHomework($list_class_lessons[$class_lesson_index]->class_id,
                                $list_class_lessons[$class_lesson_index]->lesson_id,
                                $homework_name,
                                $dead_line,
                                $additionnals_infos_changed) != -1)
                                {
                                    return HOMEWORK_ADDED_SUCCESS;
                                }else
                                {
                                    return MYSQL_ERROR;
                                }
    }

     /**
     * Affichage de l'état en fonction de l'action de l'utilisateur
     */
    public static function displayStateCode($state_code)
    {
        switch($state_code)
        {
            case WRONG_CLASS_LESSON_INDEX:
                LoadingHtmlLessons::displayMsgTmp("danger", "La classe/cours choisi est incorrect.", "margin-left:30%;margin-right:30%;", true);
                break;
            case WRONG_HOMEWORK_NAME:
                LoadingHtmlLessons::displayMsgTmp("danger", "Le nom du devoir est incorrect.", "margin-left:30%;margin-right:30%;", true);
                break;
            case WRONG_DEAD_LINE:
                LoadingHtmlLessons::displayMsgTmp("danger", "La date sélectionnée est incorrecte.", "margin-left:30%;margin-right:30%;", true);
                break;
            case WRONG_ADDITIONNALS_INFOS:
                LoadingHtmlLessons::displayMsgTmp("danger", "Les informations complétementaires sont incorrects. Lisez les instructions ci-dessus et réessayez.", "margin-left:30%;margin-right:30%;", true);
                break;
            case MYSQL_ERROR:
                LoadingHtmlLessons::displayMsgTmp("danger", "Impossible de se connecter à la base de données. Contactez l'administrateur.", "margin-left:30%;margin-right:30%;", true);
                break;
            case HOMEWORK_ADDED_SUCCESS:
                LoadingHtmlLessons::displayMsgTmp("success", "Le devoir a été ajouté avec succès.", "margin-left:30%;margin-right:30%;", true);
                break;
        }
    }
}

?>