<?php

include_once('utility/Header.php');

define('DELETE_HOMEWORK_SUCCESS', 1);
define('DELETE_HOMEWORK_FAILURE', 2);

class LoadingHtmlHomeworksManager
{
    /**
     * Affiche les devoirs concernés par l'enseignant
     */
    public static function displayHomeworks($state_code)
    {
        ManagerSession::start();
        self::displayStateCode($state_code);
?>
        <hr color="grey\" style="margin: 0 7% 0 7%;">
        <br/>
<?php
        $list_homeworks = $_SESSION['instanceMysql']->getHomeworksForManager($_SESSION['user_id']);

        foreach ($list_homeworks as $homework)
        {
            date_default_timezone_set('Europe/Paris');
            $current_date = date('Y-m-d', time());

            $color_date = "success";

            if ($homework->dead_line <= $current_date)
            {
                $color_date = "danger";
            }

            $dead_line = LoadingHtmlHomeworks::dead_lineConvert($homework->dead_line);
?>
                <div class="card border-dark mb-3" style="font-family:Calibri;font-size:16px;width:320px;height:195px;margin: 2% 4% 50px 5%;float:left;">
                    <div class="card-header" style="border-top:solid #375a7f;">

                        <button type="button" class="btn btn-danger" style="float:left;" onclick="confirmDeleteHomework(<?php echo $homework->id;?>);">Supprimer</button>

                        <button type="button" class="btn btn-success" style="float:right;" onclick="javascript:postMethod('homeworksmanager.php', 'downloadRenderingHomework', '<?php echo $homework->id;?>')">Télécharger les rendus</button></div>
            
                        <div class="card-body">
                        
                        <b><h3 class="card-title" style="margin-top:-2px;text-align:center"><?php echo $homework->name;?></h3></b>

                        <hr color="grey" style="margin: 0 7% 0 7%;">
                        <p style="margin-top:8px;" class="card-text">
                            <b style="color:#3498DB">
                                Date limite :
                            </b>
                            <h2 style="margin-top:-5px;text-align:center" class="text-<?php echo $color_date;?>">
                                <?php echo $dead_line;?>
                            </h2>
                        </p>
                        </div>
                    </div>
                </div>
            
            <form style="display: hidden" method="post" id="formDeleteHomework_<?php echo $homework->id;?>">
                <input type="hidden" name="deleteHomework" value="<?php echo $homework->id;?>"/>
            </form>

            <script>
                function confirmDeleteHomework(homework_id)
                {
                    if (confirm("Êtes-vous sûr de vouloir supprimer ce devoir ?"))
                    {
                        if (confirm("Vous ne pourrez plus accéder aux rendus de ce devoir. Supprimer quand même ?"))
                        {
                            document.getElementById('formDeleteHomework_' + homework_id).submit();
                        }
                    }
                }
            </script>
<?php
        }
    }

    /**
     * Lance le téléchargement des rendus concernant le devoir 'homework_id'
     */
    public static function downloadRenderingHomework($homework_id)
    {
        if ($_SESSION['instanceMysql']->checkHomeworkIDTeacher($homework_id, $_SESSION['user_id']))
        {
            $_SESSION['instanceFtp']->downloadRenderingHomework($homework_id);
        }
    }

    /**
     * Lance le téléchargement des rendus concernant le devoir 'homework_id' puis supprime le devoir
     * TODO : Rajouter un téléchargement des rendus avant la suppression ?
     */
    public static function deleteHomework($homework_id)
    {
        if ($_SESSION['instanceMysql']->checkHomeworkIDTeacher($homework_id, $_SESSION['user_id']))
        {
            // On supprime le dossier contenant les rendus
            $codage_homework = $_SESSION['instanceMysql']->getCodageByHomeworkID($homework_id);
            $path = PATH_HOMEWORKS . "/" . $codage_homework;
            
            $_SESSION['instanceFtp']->removeDirectoryRecursive($path);
            
            // On supprime le devoir de la base de données
            $_SESSION['instanceMysql']->deleteHomework($homework_id);
            return DELETE_HOMEWORK_SUCCESS;
        }

        return DELETE_HOMEWORK_FAILURE;
    }

     /**
     * Affichage de l'état en fonction de l'action de l'utilisateur
     */
    public static function displayStateCode($state_code)
    {
        switch ($state_code)
        {
            case DELETE_HOMEWORK_SUCCESS:
                LoadingHtmlLessons::displayMsgTmp("success", "Le devoir a bien été supprimé.", "margin-left:35%;margin-right:35%;", true);
                break;
            case DELETE_HOMEWORK_FAILURE:
                LoadingHtmlLessons::displayMsgTmp("danger", "Impossible de supprimer ce devoir.", "margin-left:35%;margin-right:35%;", true);
                break;
        }
    }
}
?>