<?php

include_once('utility/Header.php');

define('AUTHENTICATION_SUCCESS', 1);
define('AUTHENTICATION_FAILED', 0);

define('MIN_SIZE_USERNAME', 5);
define('MAX_SIZE_USERNAME', 30);

define('MIN_SIZE_PASSWORD', 4);
define('MAX_SIZE_PASSWORD', 12);

class LoadingHtmlLogin
{
    /**
     * Chargement de la page de connexion
     */
    public static function loadLoginPageClean($state_code)
    {
?>
        <div style="margin-top:-2%;" class="container">
            <div class="login-form">
                <div class="main-div">
                    <div class="panel_login">
                        <h2>Connexion à l'ENT de Baggio</h2>
                        <p>Entrez votre nom d'utilisateur et votre mot de passe.</p>
                    </div>
                    <form id="Login" method="post">
                        <div class="form-group">

                            <input type="username" class="form-control" name="input_username" placeholder="Nom d'utilisateur">

                        </div>

                        <div class="form-group">

                            <input type="password" class="form-control" name="input_password" placeholder="Mot de passe">

                        </div>
                        
                        <div class="forgot">
                            <a href="no_link.php">Mot de passe oublié ? (not work)</a>
                        </div>

                        <button type="submit" class="btn btn-primary">Se connecter</button>

                    </form>
                    <?php self::displayStateCode($state_code); ?>

                </div>

                <hr color="grey">
                <p class="bottom-text"><i> Si vous rencontrez des problèmes lors de votre connexion. Prenez contact avec l'administrateur. </i></p>
                
            </div>
        </div>
<?php
    }

    /**
     * Affichage d'un message d'information en fonction des actions de l'utilisateur
     */
    public static function displayStateCode($state_code)
    {
        switch ($state_code)
        {
            case CONNECTION_MYSQL_ERROR:
                LoadingHtmlLessons::displayMsgTmp("danger", "Serveur inaccessible", "margin: 0px -10% -15% -10%;");
                break;
            case AUTHENTICATION_FAILED:
                LoadingHtmlLessons::displayMsgTmp("danger", "Identifiants incorrects.", "margin: 0px -10% -15% -10%;");
                break;
            case POST_VARIABLES_ERROR:
                LoadingHtmlLessons::displayMsgTmp("danger", "Identifiants incorrects.", "margin: 0px -10% -16% -10%;");
                break;
        }
    }
}

?>