<?php

include_once('utility/Header.php');

class LoadingHtmlHomeworks
{
    /**
     * Chargement de la page de présentation
     */
    public static function loadIntroducingText($state_code)
    {
?>
        <?php self::displayStateCode($state_code);?>
        <hr color="grey\" style="margin: 0 7% 0 7%;">
        <br/>
        <p style="text-align:center;"><i> Un seul fichier doit être transmi par devoir (Inférieur à <?php echo FILE_SIZE_MAX_MO_HW;?> Mo) ! </i></p>
<?php
    }

    /**
     * Formatage de la date de rendu
     */
    public static function dead_lineConvert($dead_line)
    {
        $dates = explode('-', $dead_line);
        
        if (count($dates) != 3)
        {
            return "XX-XX-XXXX";
        }

        $year = $dates[0];
        $day = $dates[2];
        $month = self::nbToMonth($dates[1]);

        $date_complete = $day . " " . $month . " " . $year;

        return $date_complete;
    }

    /**
     * Affichage des devoirs
     */
    public static function addListingHomeworks($user_id, $list_homeworks)
    {
        $form_id = 0;
        foreach ($list_homeworks as $homework)
        {
            $form_id++;
            $dead_line = self::dead_lineConvert($homework->dead_line);

            $already_sent = $_SESSION['instanceFtp']->homeworkAlreadySent($homework->codage, $user_id);

            $class_name = $_SESSION['instanceMysql']->getClassNameByClassID($homework->class_id);

            $header_homework = $homework->lesson_name . " ($class_name)";

            self::addHomeworks($header_homework, $homework->name, $dead_line, $homework->messages, $homework->codage, $form_id, $already_sent);    
        }
    }

    /**
     * Ajout d'un devoir dans la fenêtre
     */
    public static function addHomeworks($header, $title, $dead_line, $messages, $codage, $form_id, $already_sent)
    {
        $style = "font-family:Calibri; font-size:16px; width:350px; height:500px; margin: 2% 4% 10px 45px; float:left;";

        $type_border = ($already_sent) ? "success" : "secondary";

        echo "<div class=\"card border-$type_border mb-3\" style=\"$style\">
                <div class=\"card-header\">
                    <h4 style=\"font-size:17px;float:left\">$header</h4>";

                    if ($already_sent)
                    {
?>
                    <div class="btn-group" style="float:right;">
                        <a href="#" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            Mon devoir
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                        <li><a class="dropdown-item" href="javascript:postMethod('homeworks.php', 'downloadHomework', '<?php echo $codage;?>')">Télécharger mon devoir</a></li>
                        <li><a class="dropdown-item" href="javascript:postMethod('homeworks.php', 'deleteHomework', '<?php echo $codage;?>')">Supprimer mon devoir</a></li>
                        </ul>
                        </div>
<?php
                    }else
                    {
?>
                        <button type="button" class="btn btn-primary" style="float:right;" onclick="homeworkFormSubmit(<?php echo $form_id;?>);">Soumettre</button>
<?php
                    }
?>
            </div>     
                <div class="card-body">
                    <b><h4 class="card-title" style="text-align:center"><?php echo $title;?></h4></b>
                    <hr color="grey" style="margin: 0 7% 0 7%;">
                    <p class="card-text">
                        <p>
                            <b style="color:#3498DB">
                                Date limite :
                            </b>
                                <br/>
                                <?php echo $dead_line;?>
                                <br/>
                        </p>

                        <b style="color:#3498DB;">
                            Informations complémentaires :
                        </b>
                            <br/>
                            <?php echo $messages;?>
                    </p>
                </div>

                <hr color="grey" style="margin-top: -6%;">
<?php
                if ($already_sent)
                {
?>
                    <i><h3 class="text-muted" style="color:white;text-align:center;margin-top:-3%;">Le devoir a été rendu.</h3></i>
<?php
                }else
                {
?>
                    <form method="post" id="homeworkForm_<?php echo $form_id; ?>" enctype="multipart/form-data">
                            <div class="form-group" style="padding: 0 3% 0 9%;">
                                <input type="hidden" name="codage" value="<?php echo $codage; ?>">
                                <input style="color:white;" name="fileHomework" type="file" class="form-control-file">
                            </div>
                        </form>
<?php
                }
?>
                    </div>

                    <script>
                    function homeworkFormSubmit(form_id)
                    {
                        document.getElementById("homeworkForm_" + form_id).submit();
                    }
                    </script>
<?php
    }

    /**
     * Lancement du téléchargement d'un devoir rendu
     */
    public static function downloadHomework($codage)
    {
        $user_id = $_SESSION['user_id'];
        $homework_sent = $_SESSION['instanceFtp']->homeworkAlreadySent($codage, $user_id);

        if ($homework_sent)
        {
            $filename = $_SESSION['instanceFtp']->getFilenameHomeworkSent($codage, $user_id);
            
            $_SESSION['instanceFtp']->downloadFile($filename, false);
        }
    }

    /**
     * Suppression d'un devoir rendu
     */
    public static function deleteHomework($codage)
    {
        $user_id = $_SESSION['user_id'];
        $homework_sent = $_SESSION['instanceFtp']->homeworkAlreadySent($codage, $user_id);

        if ($homework_sent)
        {
            $folder_path = PATH_HOMEWORKS . "/$codage/$user_id";

            $_SESSION['instanceFtp']->removeDirectoryRecursive($folder_path);
        }
    }

    /**
     * Vérification du codage (3 chiffres entier separer par un underscore)
     */
    public static function checkCodage($codage)
    {
        $codage_id = explode('_', $codage);
        if (count($codage_id) != 3)
        {
            return false;
        }

        return ctype_digit($codage_id[0]) && ctype_digit($codage_id[1]) && ctype_digit($codage_id[2]);
    }

    /**
     * Récuperation d'un tableau à 3 élements (pour chaque parti du codage)
     */
    public static function getCodageArray($codage)
    {
        if (self::checkCodage($codage))
        {
            return explode('_', $codage);
        }
    }

    /**
     * Formatage du mois par son index
     */
    public static function nbToMonth($nb)
    {
        switch ($nb)
        {
            case 1:
                return "Janvier";
            case 2:
                return "Février";
            case 3:
                return "Mars";
            case 4:
                return "Avril";
            case 5:
                return "Mai";
            case 6:
                return "Juin";
            case 7:
                return "Juillet";
            case 8:
                return "Août";
            case 9:
                return "Septembre";
            case 10:
                return "Octobre";
            case 11:
                return "Novembre";
            case 12:
                return "Décembre";
        }
        return "undefined";
    }
    
    /**
     * Affichage d'un message d'état en fonction de l'action de l'utilisateur
     */
    public static function displayStateCode($state_code)
    {
        switch ($state_code)
        {
            case UPLOAD_FILE_SUCCESS:
                LoadingHtmlLessons::displayMsgTmp("success", "Votre devoir a été rendu avec succès", "width:auto;margin-left:30%;margin-right:30%;", true);
                break;
            case UPLOAD_FILE_FAILED:
                LoadingHtmlLessons::displayMsgTmp("danger", "Vérifiez que le fichier transmi soit correct.", "margin-left:30%;margin-right:30%;");
                break;
            case UPLOAD_FILE_ALREADY_EXISTS: // Normalement improbable car on veut uniquement 1 fichier par dossier de devoir
                LoadingHtmlLessons::displayMsgTmp("danger", "Le fichier ajouté existe déjà.", "margin-left:30%;margin-right:30%;");
                break;
            case UPLOAD_FILE_TOO_BIG:
                LoadingHtmlLessons::displayMsgTmp("danger", "Le fichier doit être inférieur à " . FILE_SIZE_MAX_MO_HW . " Mo !", "margin-left:30%;margin-right:30%;");
                break;
        }
    }
}

?>