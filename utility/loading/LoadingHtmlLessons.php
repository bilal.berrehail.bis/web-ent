<?php

include_once('utility/Header.php');

define('DIRECTORY_COUNT_FOR_LESSONS', '3');

class LoadingHtmlLessons
{
    /**
     * Affichage de la barre de navigation du dossier FTP " Cours "
     */
    public static function infosPath()
    {
        $list_folders = explode('/', $_SESSION['path_lessons']);
        unset($list_folders[0]);

        $index_folder = 0;

        $count_folders = count($list_folders) - 1;

        foreach ($list_folders as $folder)
        {
            if ($index_folder == $count_folders)
            {
                echo "<li class=\"breadcrumb-item active\">$folder</li>";
            }else
            {
                echo "<li class=\"breadcrumb-item\"><a href=\"javascript:postMethod('lessons.php', 'Path_Back', '" . ($count_folders - $index_folder) . "')\">$folder</a></li>";
            }
            $index_folder++;
        }
    }

    /**
     * Affichage de tous les élements contenus dans un dossier
     */
    public static function displayPath()
    {
        $count_directory = self::getCountDirectory($_SESSION['path_lessons']);

        if ($count_directory == 0 || $count_directory == 2)
        {
            self::displayClass();
        }else if ($count_directory >= DIRECTORY_COUNT_FOR_LESSONS)
        {
            if (self::classIsCorrectForUser($_SESSION['path_lessons']))
            {
                if ($count_directory == DIRECTORY_COUNT_FOR_LESSONS)
                {
                    self::displayLessons();
                }else if ($count_directory >= DIRECTORY_COUNT_FOR_LESSONS + 1)
                {
                    if (self::lessonIsCorrectForUser($_SESSION['path_lessons']))
                    {
                        self::displayFilesInFolder();
                    }
                }
            }
        }
    }

    /**
     * Affichage des dossiers et des fichiers contenus dans un dossier (Dossier en premier | Fichiers en dernier)
     */
    public static function displayFilesInFolder()
    {
        $list_files_ftp = $_SESSION['instanceFtp']->getListFilesInFolder($_SESSION['path_lessons']);

        foreach ($list_files_ftp as $path_file) // Affiche les dossiers
        {
            if ($_SESSION['instanceFtp']->isFolder($path_file))
            {
                self::insertLine($path_file);
            }
        }

        foreach ($list_files_ftp as $path_file) // Affiche les fichiers
        {
            if (!$_SESSION['instanceFtp']->isFolder($path_file))
            {
                self::insertLine($path_file);
            }
        }
    }

    /**
     * Affichage des cours concerné par l'utilisateur
     */
    public static function displayLessons()
    {
        $list_lessons_ftp = $_SESSION['instanceFtp']->getListFilesInFolder($_SESSION['path_lessons']);
        $get_lessons = $_SESSION['instanceMysql']->getLessonsByUserID($_SESSION['user_id']);

        foreach($list_lessons_ftp as $path_lesson_ftp)
        {
            $lesson_ftp = basename($path_lesson_ftp);
            if (self::lessonExistInLessons($get_lessons, $lesson_ftp))
            {
                self::insertLine($path_lesson_ftp);
            }
        }
    }

    /**
     * Affichage des classes concerné par l'utilisateur
     */
    public static function displayClass()
    {
        $list_class_ftp = $_SESSION['instanceFtp']->getListFilesInFolder($_SESSION['path_lessons']);
        $list_class_name = $_SESSION['instanceMysql']->getClassNameByUserID($_SESSION['user_id']);

        foreach ($list_class_ftp as $path_class_ftp)
        {
            foreach($list_class_name as $get_class_name)
            {
                $class_ftp = basename($path_class_ftp);

                if ($class_ftp == $get_class_name)
                {
                    self::insertLine($path_class_ftp);
                }
            }
        }
    }

    /**
     * Récuperation du nom de la classe à l'aide de son chemin de fichier dans le dossier FTP " Cours "
     */
    public static function getClassFromPathFtp($path_lessons)
    {
        $chemins = explode('/', $path_lessons);

        if (count($chemins) >= DIRECTORY_COUNT_FOR_LESSONS - 1)
        {
            return $chemins[DIRECTORY_COUNT_FOR_LESSONS - 1];
        }
    }

    /**
     * Récuperation du chemin de fichier pointant vers le dossier de la classe de l'utilisateur
     */
    public static function getClassPathFromPathFtp($path_lessons)
    {
        $chemins = explode('/', $path_lessons);

        if (count($chemins) > DIRECTORY_COUNT_FOR_LESSONS)
        {
            $path_class = "";

            for ($i = 0; $i <= DIRECTORY_COUNT_FOR_LESSONS; $i++)
            {
                $path_class .= $chemins[$i];
                $path_class .= "/";
            }

            return $path_class;
        }
    }

    /**
     * Vérification de la classe de l'utilisateur (S'il se trouve dans un dossier dont il en a l'autorisation)
     */
    public static function classIsCorrectForUser($path_lessons)
    {
        $list_class_name = $_SESSION['instanceMysql']->getClassNameByUserID($_SESSION['user_id']);

        $class_from_path_lessons = self::getClassFromPathFtp($path_lessons);

        foreach ($list_class_name as $get_class_name)
        {
            if (strcmp($get_class_name, $class_from_path_lessons) == 0)
            {
                return true;
            }
        }
    }

    /**
     * Vérification du cours de l'utilisateur (S'il se trouve dans un dossier dont il en a l'autorisation)
     */
    public static function lessonIsCorrectForUser($path_lessons)
    {
        $get_lessons = $_SESSION['instanceMysql']->getLessonsByUserID($_SESSION['user_id']);

        $lesson_from_path_lessons = explode('/', $path_lessons)[DIRECTORY_COUNT_FOR_LESSONS];

        return self::lessonExistInLessons($get_lessons, $lesson_from_path_lessons);
    }

    /**
     * Si le cours existe dans une liste de cours
     */
    public static function lessonExistInLessons($lessons, $lesson_name)
    {
        foreach ($lessons as $lesson)
        {
            if ($lesson == $lesson_name)
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Evenement lorsque l'utilisateur clique sur un élement 
     */
    public static function appendFolderInPath($folder)
    {
        $full_path_lessons = $_SESSION['path_lessons'] . "/" . $folder;
        $url_ftp_file = $_SESSION['instanceFtp']->getUrlByPath($full_path_lessons);
       
        if ($_SESSION['instanceFtp']->isFolder($full_path_lessons))
        {
            if ($_SESSION['instanceFtp']->getListFilesInFolder($full_path_lessons) != 0)
            {
                if (self::getCountDirectory($full_path_lessons) >= DIRECTORY_COUNT_FOR_LESSONS)
                {
                    if (self::classIsCorrectForUser($full_path_lessons))
                    {
                        if (self::getCountDirectory($full_path_lessons) >= DIRECTORY_COUNT_FOR_LESSONS + 1)
                        {
                            if (self::lessonIsCorrectForUser($full_path_lessons)) // Il doit avoir la bonne leçon
                            {
                                $_SESSION['path_lessons'] = $full_path_lessons;
                            }
                        }else // Il n'a pas encore selectionné de leçon
                        {
                            $_SESSION['path_lessons'] = $full_path_lessons;
                        }
                    }
                }
            }
        }else // Si c'est un fichier .. le télécharger
        {
            $list_files_ftp = $_SESSION['instanceFtp']->getListFilesInFolder($_SESSION['path_lessons']);
            $filename = $folder;

            foreach ($list_files_ftp as $files)
            {
                if (basename($files) == $filename)
                {
                    $_SESSION['instanceFtp']->downloadFile($full_path_lessons, false); // Par defaut, le téléchagement se fait avec les droits d'un élève, puisque le téléchargement est possible avec.
                    exit;
                }
            }
        }
    }

    /**
     * Evenement lorsque l'utilisateur retourne en arrière dans son arborescence
     */
    public static function reduceFolderInPath($folder_id)
    {
        $path_lessons = $_SESSION['path_lessons'];
        $list_folders = explode('/', $path_lessons);

        $count_list_folders = count($list_folders) - 1;

        if ($count_list_folders - $folder_id < 1 || $folder_id >= $count_list_folders)
        {
            return;
        }

        for ($i = $count_list_folders; $i > $count_list_folders - $folder_id; $i--)
        {
            unset($list_folders[$i]);
        }

        $path_lessons = "";

        foreach ($list_folders as $folder)
        {
            $path_lessons .= $folder;
            $path_lessons .= "/";
        }

        $path_lessons = substr_replace($path_lessons, "", -1);

        $_SESSION['path_lessons'] = $path_lessons;
    }

    /**
     * Récuperation du nombre de dossier parcouru
     */
    public static function getCountDirectory($path)
    {
        return count(explode('/', $path));
    }

    /**
     * Recuperation de l'extnesion d'un fichier
     */
    public static function getExtensionFile($file_name)
    {
        $explode_file = explode('.', $file_name);

        if (count($explode_file) == 1)
        {
            return "";
        }

        return $explode_file[count($explode_file) - 1];
    }

    /**
     * Affichage d'un element dans un répertoire
     */
    public static function insertLine($path_file_name)
    {
        $img_name = "";
        $type_line = "";
        $add_style_list = "";

        $is_folder = $_SESSION['instanceFtp']->isFolder($path_file_name);

        if ($is_folder)
        {
            $img_name = "folder";
        }else
        {
            $get_extensions = strtolower(self::getExtensionFile(basename($path_file_name)));

            switch (true)
            {
                case !strcmp($get_extensions, "txt"):
                    $img_name = "txt_file.png";
                    break;
                case (!strcmp($get_extensions, "rar") || !strcmp($get_extensions, "zip") || !strcmp($get_extensions, "gz")):
                    $img_name = "rar_file.png";
                    break;
                case !strcmp($get_extensions, "pdf"):
                    $img_name = "pdf_file.png";
                    break;
                case (!strcmp($get_extensions, "jpg") || !strcmp($get_extensions, "jpeg") || !strcmp($get_extensions, "png") || !strcmp($get_extensions, "bmp")):
                    $img_name = "img_file.png";
                    break;
                case !strcmp($get_extensions, "mp4") || !strcmp($get_extensions, "avi") || !strcmp($get_extensions, "mov") || !strcmp($get_extensions, "flv") || !strcmp($get_extensions, "wmv"):
                    $img_name = "video_file.png";
                    break;
                case !strcmp($get_extensions, "exe"):
                    $img_name = "exe_file.png";
                    break;
                case !strcmp($get_extensions, "dll"):
                    $img_name = "dll_file.png";
                    break;
                default:
                    $img_name = "default_file.png";
                    break;
            }
        }
        
        $type_line = $is_folder ? "default" : "default";

        $file_name = basename($path_file_name);

        $is_teacher = $_SESSION['isTeacher'];

        if ($is_teacher && self::getCountDirectory($path_file_name) >= DIRECTORY_COUNT_FOR_LESSONS + 2) // Uniquement son dossier
        {
            $add_style_list .= "margin-left:4%;width:96%;";
            echo "<a  href=\"javascript:postMethod('lessons.php', 'Path_Delete', '$path_file_name')\"><img width=\"24\" height=\"24\" src=\"img/icons/delete_icon.png\" style=\"position:absolute; margin: 15px 0px 0px 11px;\"></a>";
        }

        echo "<a style=\"$add_style_list\" href=\"javascript:postMethod('lessons.php', 'Path_Next', '$file_name')\" class=\"list-group-item list-group-item-action list-group-item-$type_line\"><img src=\"img/icons/$img_name\" width=\"22\" height=\"22\">&nbsp $file_name</a>";
    }

    /**
     * Chargement du serveur de création de fichiers (Uniquement pour les professeurs)
     */
    public static function loadServiceCreateFiles()
    {
        $actual_path = $_SESSION['path_lessons'];
        $is_teacher = $_SESSION['isTeacher'];

        if ($is_teacher && self::getCountDirectory($actual_path) >= DIRECTORY_COUNT_FOR_LESSONS + 1) // Uniquement son dossier
        {
            $files_size_all_mo = $_SESSION['instanceFtp']->getFileSizeByDirectory(self::getClassPathFromPathFtp($actual_path)) / 1000000;
?>
            (Dossier de cours) La somme de la taille de chaque fichier doit être inférieur à <?php echo FILES_SIZE_MAX_LESSON_MO;?> Mo. (Taille actuel : <?php echo round($files_size_all_mo, 2);?> Mo)

            <hr color="grey"/><br/>
            <form method="post" style="display:inline;margin-left:41%;" class="form-inline my-2 my-lg-0">
                <input class="form-control" name="Create_Folder" type="text" placeholder="Créer un dossier">
                <button class="btn btn-success my-2 my-sm-0" type="submit">Créer</button>
            </form>
            
            <br/><br/>
            <form method="post" style="margin-left:32%;" class="form-inline my-2 my-lg-0" enctype="multipart/form-data">
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="btn btn-primary btn-file">
                            Parcourir&hellip; <input name="Fichier_Upload[]" type="file" multiple data-show-upload="true" data-show-caption="true">
                        </span>
                    </span>
                    <input type="text" class="form-control" readonly>
                </div>
                <button class="btn btn-success my-2 my-sm-0" type="submit">Ajouter les fichiers</button>
            </form>
            <br/>
            <footer style="text-align:center;" class="blockquote-footer"><i><p style="color:white">Fichiers de taille inférieures à <?php echo FILE_SIZE_MAX_MO?> Mo<br>Limite de 20 fichiers à la fois</p></i></footer>
            <br/><br/>
<?php
        }
    }

    /**
     * Affichage de l'état en fonction des choix de l'utilisateur
     */
    public static function displayStateCode($state_code)
    {
        $current_path = $_SESSION['path_lessons'];

        if (LoadingHtmlLessons::getCountDirectory($current_path) >= DIRECTORY_COUNT_FOR_LESSONS + 1) // + 1 car on veut le repértoire courant et non agir sur un fichier/dossier à l'intérieur de celui-ci
        {
            if (LoadingHtmlLessons::classIsCorrectForUser($current_path))
            {
                if (LoadingHtmlLessons::lessonIsCorrectForUser($current_path))
                {
                    switch ($state_code)
                    {
                        case PATH_DELETE_SUCCESS:
                            self::displayMsgTmp("success", "Vous venez de supprimer un élement de ce répertoire.", "margin-left:30%;margin-right:30%");
                            break;
                        case PATH_DELETE_FAILED:
                            self::displayMsgTmp("danger", "Impossible de supprimer cet élement de ce répertoire.", "margin-left:30%;margin-right:30%");
                            break;
                        case CREATE_FOLDER_SUCCESS:
                            self::displayMsgTmp("success", "Vous venez de créer un nouveau dossier dans ce répertoire.", "margin-left:30%;margin-right:30%");
                            break;
                        case CREATE_FOLDER_FAILED:
                            self::displayMsgTmp("danger", "Impossible de créer un nouveau dossier dans ce répertoire. Vérifiez que le nom utilisé est correct.", "margin-left:30%;margin-right:30%");
                            break;
                        case UPLOAD_FILE_SUCCESS:
                            self::displayMsgTmp("success", "Vous venez d'ajouter un/des fichier(s) dans ce répertoire.", "margin-left:30%;margin-right:30%");
                            break;
                        case UPLOAD_FILE_FAILED:
                            self::displayMsgTmp("danger", "Impossible d'ajouter de fichiers dans ce répertoire. (Fichier trop lourd ou nom du fichier invalide).", "margin-left:30%;margin-right:30%");
                            break;
                        case UPLOAD_FILE_ALREADY_EXISTS:
                            self::displayMsgTmp("danger", "Le fichier existe déjà.", "margin-left:30%;margin-right:30%");
                            break;
                        case UPLOAD_FILE_TOO_BIG:
                            self::displayMsgTmp("danger", "Le fichier doit être inférieur à " . FILE_SIZE_MAX_MO . " Mo.", "margin-left:30%;margin-right:30%");
                            break;
                        case UPLOAD_FILES_LESSON_SIZE_DIRECTORY_TOO_BIG:
                            self::displayMsgTmp("danger", "L'ensemble des fichiers dans votre dossier de cours ne peut dépasser " . FILES_SIZE_MAX_LESSON_MO . " Mo. Supprimez des fichiers pour en ajouter d'autres.", "margin-left:30%;margin-right:30%");
                            break;
                    }
                }
            }
        }
    }

    /**
     * Affichage d'un message d'informations pouvant être supprimé (Par le script)
     */
    public static function displayMsgTmp($state, $msg, $style = "", $no_prefixe = false)
    {
        $state_prefixe = "Reussi!";
        if ($state == "danger")
            $state_prefixe = "Erreur!";

        if ($no_prefixe)
            $state_prefixe = "";

        echo "<div id=\"tmp_msg\" class=\"alert alert-dismissible alert-$state\" style=\"$style\">
                <button type=\"button\" class=\"close\" data-dismiss=\"alert\" onclick=\"removeMsgTmp()\">&times;</button>
                <strong>$state_prefixe</strong> $msg
             </div>";
    }

}

?>