<?php

include_once('Header.php');

class ManagerSession
{
    /**
     * Lancement d'une session
     */
    public static function start()
    {
        if (!self::isStart()) 
        {
            session_start();
            $_SESSION['instanceMysql'] = ManagerMysql::getInstance();
            $_SESSION['isTeacher'] = self::isTeacher();
            $_SESSION['instanceFtp'] = ManagerFtp::getInstance($_SESSION['isTeacher']);
            return true;
        }

        return false;
    }

    /**
     * Vérification si une session est ouverture
     */
    public static function isStart()
    {
        $state = session_status() == PHP_SESSION_ACTIVE ? true : false;

        return $state;
    }

    /**
     * Vérification si l'utilisateur est déjà connecté
     */
    public static function isLogged()
    {
        return isset($_SESSION['user_id']);
    }

    /**
     * Déconnexion de la session utilisateur
     */
    public static function logout()
    {
        if (self::isLogged())
        {
            session_unset();

            session_destroy();
            return true;
        }
        return false;
    }

    /**
     * Ajout d'une valeur (id_value) à une variable de session (ici : user_id)
     */
    public static function setValue($id_value)
    {
        if (self::isStart())
        {
            if (!self::isLogged())
            {
                session_regenerate_id(true);
                
                $_SESSION['user_id'] = $id_value;
               
                return true;
            }
        }
        return false;
    }
    
    /**
     * Vérification si l'utilisateur est un professeur ou non
     */
    public static function isTeacher()
    {
        if (!self::isLogged())
        {
            return false;
        }
        return ($_SESSION['instanceMysql']->getPermissionsGradeByUserID($_SESSION['user_id']) == 1);
    }
}

?>