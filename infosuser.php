<?php

include_once('utility/Header.php');
ManagerSession::start();

$isLogged = ManagerSession::isLogged();

if (!$isLogged) // Il doit nécéssairement être connecté
{
    return;
}

$user_id = $_SESSION['user_id'];

?>

<!DOCTYPE html>

<html>
    <head>

        <?php loadingHtmlNavbar::loadHeadContent(); ?>

    </head>

    <body>

        <?php loadingHtmlNavbar::loadNavbar(0); ?>
        <?php loadingHtmlInfosUser::loadInformations(); ?>

    </body>

</html>