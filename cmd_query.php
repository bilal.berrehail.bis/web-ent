<?php

include_once('utility/Header.php');
ManagerSession::start();

$isLogged = ManagerSession::isLogged();

if (!$isLogged) // Il doit nécéssairement être connecté
{
    return;
}

$user_id = $_SESSION['user_id'];

$get_permissions_grade = $_SESSION['instanceMysql']->getPermissionsGradeByUserID($user_id);

if ($get_permissions_grade != 2) // Administrateur uniquement
{
    return;
}

ob_clean();
flush();

// add_user <username> <password> <grade>
// remove_user <username>
// add_class <class_name>
// add_lesson <lesson_name>
// show_users
// show_class
// show_lessons
// add_user_class_lesson <user_id> <class_id> <lesson_id>

if (isset($_POST['add_user']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['grade']))
{
    ManagerCmd::add_user($_POST['username'], $_POST['password'], $_POST['grade']);
}
else if (isset($_POST['remove_user']) && isset($_POST['username']))
{
    ManagerCmd::remove_user($_POST['username']);
}
else if (isset($_POST['add_class']) && isset($_POST['class_name']))
{
    ManagerCmd::add_class($_POST['class_name']);
}
else if (isset($_POST['add_lesson']) && isset($_POST['lesson_name']))
{
    ManagerCmd::add_lesson($_POST['lesson_name']);
}
else if (isset($_POST['show_users']))
{
    ManagerCmd::show_users();
}
else if (isset($_POST['show_class']))
{
    ManagerCmd::show_class();
}
else if (isset($_POST['show_lessons']))
{
    ManagerCmd::show_lessons();
}
else if (isset($_POST['add_user_class_lesson']) && isset($_POST['user_id']) && isset($_POST['class_id']) && isset($_POST['lesson_id']))
{
    ManagerCmd::add_user_class_lesson($_POST['user_id'], $_POST['class_id'], $_POST['lesson_id']);
}
?>		