<?php

include_once('utility/Header.php');
ManagerSession::start();

$isLogged = ManagerSession::isLogged();

if (!$isLogged) // Il doit nécéssairement être connecté
{
    return;
}

$user_id = $_SESSION['user_id'];

$get_permissions_grade = $_SESSION['instanceMysql']->getPermissionsGradeByUserID($user_id);

if ($get_permissions_grade != 1) // Enseignant uniquement
{
    return;
}

$state_code = -1;

if (isset($_POST['additionnals_infos']) && isset($_POST['homework_name']) && isset($_POST['dead_line']) && isset($_POST['class_lesson_index']))
{
    $state_code = loadingHtmlHomeworksAdmin::createHomeworkHandle($_POST['class_lesson_index'], $_POST['homework_name'], $_POST['dead_line'], $_POST['additionnals_infos']);
}

?>

<!DOCTYPE html>

<html>
    <head>

        <?php loadingHtmlNavbar::loadHeadContent(); ?>

        <link href="css/datepicker.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="js/datepicker.js"></script>
        
    </head>

    <body>

        <?php loadingHtmlNavbar::loadNavbar(3); ?>

        <?php loadingHtmlHomeworksAdmin::loadCreateHomeworkService($state_code); ?>

    </body>

</html>