<?php

include_once('utility/Header.php');

ManagerSession::start();

$isLogged = ManagerSession::isLogged();

if (!$isLogged) // Il doit nécessairement être connecté
{
    return;
}

$secret = "6Le2FaQUAAAAAMyk9sEgqosnreQ6qtvoBq3zuAYg";

$response = $_POST['g-recaptcha-response'];

$remoteip = $_SERVER['REMOTE_ADDR'];

$api_url = "https://www.google.com/recaptcha/api/siteverify?secret=" 
    . $secret
    . "&response=" . $response
    . "&remoteip=" . $remoteip ;

$decode = json_decode(file_get_contents($api_url), true);

$state_code = "";

if (isset($_POST['input_old_pwd']) && isset($_POST['input_pwd']) && isset($_POST['input_confirm_pwd']))
{
    if ($decode['success'] == true)
    {
        $input_old_pwd = $_POST['input_old_pwd'];
        $input_pwd = $_POST['input_pwd'];
        $input_confirm_pwd = $_POST['input_confirm_pwd'];
        $user_id_tmp = -1;
            
        $get_username = $_SESSION['instanceMysql']->getUsernameByUserID($_SESSION['user_id']);

        if ($_SESSION['instanceMysql']->checkAuthentication($get_username, $input_old_pwd, $user_id_tmp) == AUTHENTICATION_SUCCESS)
        {
            if (!strcmp($input_pwd, $input_confirm_pwd))
            {
                if (!strcmp($input_old_pwd, $input_pwd))
                    $state_code = PWD_SAME;
                else
                {
                    if ($_SESSION['instanceMysql']->checkPassword($input_pwd))
                        $state_code = $_SESSION['instanceMysql']->updatePwd($_SESSION['user_id'], $input_pwd);
                    else
                        $state_code = PWD_BAD;
                }
            }else
                $state_code = PWD_DIFFERENT;
        }else
            $state_code = PWD_INCORRECT;
    }
    else
        $state_code = ERROR_CAPTCHA;
}

?>

<!DOCTYPE html>

<html>
    <head>

        <?php loadingHtmlNavbar::loadHeadContent(); ?>
        <link href="css/login.css" rel="stylesheet">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
    </head>

    <body>

        <?php loadingHtmlNavbar::loadNavbar(0); ?>
        <br/>
        <?php LoadingHtmlChangePwd::loadForm($state_code); ?>

    </body>

</html>