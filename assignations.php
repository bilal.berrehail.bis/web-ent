<?php

include_once('utility/Header.php');
ManagerSession::start();

$isLogged = ManagerSession::isLogged();

if (!$isLogged) // Il doit nécéssairement être connecté
{
    return;
}

$user_id = $_SESSION['user_id'];

$get_permissions_grade = $_SESSION['instanceMysql']->getPermissionsGradeByUserID($user_id);

if ($get_permissions_grade != 2) // Administrateur uniquement
{
    return;
}
?>

<!DOCTYPE html>

<html>
    <head>

        <?php loadingHtmlNavbar::loadHeadContent(); ?>

    </head>

    <body>

        <?php loadingHtmlNavbar::loadNavbar(4); ?>
        <?php loadingHtmlAssignations::loadDefaultPage(); ?>

    </body>

</html>