var commands = ['add_user',
                'remove_user',
                'add_class',
                'add_lesson',
                'show_users',
                'show_class',
                'show_lessons',
                'add_user_class_lesson',
                'help']; // Pas utilisé pour le moment

var rlwrap = [];
var currentIndexRlwrap = 0;

function domLoaded()
{
    $("#input").on( "keydown", (event) => 
    {
        if (event.key === "Enter")
        {
            let input = document.getElementById('input').value;

            if (input != '')
            {
                rlwrap.push(input);
                currentIndexRlwrap = rlwrap.length;
                document.getElementById('input').value = '';
                getDataResponse(input);
            }
        }
        else if (event.key === "ArrowUp")
        {
            if (currentIndexRlwrap > 0)
            {
                currentIndexRlwrap--;
                document.getElementById('input').value = rlwrap[currentIndexRlwrap];
            }
        }
        else if (event.key === "ArrowDown")
        {
            if (currentIndexRlwrap < rlwrap.length - 1)
            {
                currentIndexRlwrap++;
                document.getElementById('input').value = rlwrap[currentIndexRlwrap];
            }else
            {
                document.getElementById('input').value = '';
                currentIndexRlwrap = rlwrap.length;
            }
        }
   });
}

function getDataResponse(input)
{
    let input_split = input.split(' ');

    input_split = input_split.filter(elem => elem != '');

    let data_build = '';
    
    switch (input_split[0])
    {
        case 'add_user':
            if (input_split.length == 4)
                data_build = 'add_user=&username=' + input_split[1] + '&password=' + input_split[2] + '&grade=' + input_split[3];
            else
            {
                handleCommand('info Vérifiez la syntaxe : add_user [username] [password] [grade] !');
                return;
            }
            break;
        case 'remove_user':
            if (input_split.length == 2)
                data_build = 'remove_user=&username=' + input_split[1];
            else
            {
                handleCommand('info Vérifiez la syntaxe : remove_user [username]');
                return;
            }
            break;
        case 'add_class':
            if (input_split.length >= 2)
            {
                data_build = 'add_class=';
                for (let i = 1; i < input_split.length; i++)
                {
                    data_build += '&class_name[]=' + input_split[i];
                }
            }
            else
            {
                handleCommand('info Vérifiez la syntaxe : add_class [class_name]');
                return;
            }
            break;
        case 'add_lesson':
            if (input_split.length >= 2)
            {
                data_build = 'add_lesson=';
                for (let i = 1; i < input_split.length; i++)
                {
                    data_build += '&lesson_name[]=' + input_split[i];
                }
            }
            else
            {
                handleCommand('info Vérifiez la syntaxe : add_lesson [lesson_name]');
                return;
            }
            break;
        case 'show_users':
            if (input_split.length == 1)
            {
                data_build = 'show_users';
            }else
            {
                handleCommand('info Vérifiez la syntaxe : show_users');
                return;
            }
            break;
        case 'show_class':
            if (input_split.length == 1)
            {
                data_build = 'show_class';
            }else
            {
                handleCommand('info Vérifiez la syntaxe : show_class');
                return;
            }
            break;
        case 'show_lessons':
            if (input_split.length == 1)
            {
                data_build = 'show_lessons';
            }else
            {
                handleCommand('info Vérifiez la syntaxe : show_lessons');
                return;
            }
            break;
        case 'add_user_class_lesson':
            if (input_split.length == 4)
            {
                data_build = 'add_user_class_lesson&user_id=' + input_split[1] + '&class_id=' + input_split[2] + '&lesson_id=' + input_split[3];
            }else
            {
                handleCommand('info Vérifiez la syntaxe : add_user_class_lesson [user_id] [class_id] [lesson_id] !');
                return;
            }
            break;
        case 'help':
            buildHelpRender();
            break;
        default:
            handleCommand('secondary La commande entrée n\'existe pas !');
            break;
    }

    $.ajax({
        url : 'cmd_query.php',
        type : 'POST', // Le type de la requête HTTP, ici devenu POST
        data : data_build,
        dataType : 'text',
        success : function(code_html)
        {
            handleCommand(code_html);
        },
        error : () => handleCommand('danger L\'invite de commande est inutilisable')
    });
}

function buildHelpRender()
{
    handleCommand('default <table style="font-size:13px;" class="table table-striped table-hover">' + 
        '<thead> <tr> <td>Commande</td> <td>Description</td> </tr> </thead>' +
        '<tbody>' +
        '<tr><td>add_user [username] [password] [grade]</td> <td>Ajouter un utilisateur</td></tr>' +
        '<tr><td>remove_user [username]</td> <td>Supprime un utilisateur</td></tr>' +
        '<tr><td>add_class [classe_name]</td> <td>Ajoute une classe</td></tr>' +
        '<tr><td>add_lesson [lesson_name]</td> <td>Ajoute une matière</td></tr>' +
        '<tr><td>show_users</td> <td>Affiche les utilisateurs</td></tr>' +
        '<tr><td>show_class</td> <td>Affiche les classes</td></tr>' +
        '<tr><td>show_lessons</td> <td>Affiche les matières</td></tr>' +
        '<tr><td>add_user_class_lesson [user_id] [class_id] [lesson_id]</td> <td>Attribuer à un utilisateur une classe et une matière</td></tr>' +
        '</tbody> </table>');
}

function handleCommand(msg_received)
{
    let group_msg_received = msg_received.split('|');
    for (let i = 0; i < group_msg_received.length; i++)
    {
        let msg_received_split = group_msg_received[i].split(' ');

        if (msg_received_split.length > 1)
        {
            let status = msg_received_split[0];
            msg_received_split.shift();

            group_msg_received[i] = group_msg_received[i].substring(status.length + 1);
    
            // Gère l'affichage
            let output = document.getElementById("output");
        
            let add_output = document.createElement('li');
            add_output.setAttribute('class', 'list-group-item list-group-item-' + status);
            add_output.innerHTML = group_msg_received[i];
        
            output.appendChild(add_output);
        
            output.scrollTo(0, output.scrollHeight)
        }
    }
}

document.addEventListener("DOMContentLoaded", domLoaded);

function removeMsgTmp() 
{ 
    document.getElementById('tmp_msg').remove(); 
}

function postMethod(url, key, value)
{
    var form = $('<form action="' + url + '" method="post">' +
            '<input type="hidden" name="' + key + '" value="' + value + '"/>' +
            '</form>');
    $('body').append(form);
    $(form).submit();
}