function createAccountsUser()
{
    let infosusersInput = document.getElementById('infousers').value;
    
    document.getElementById("successaccount").value = "";
    document.getElementById("failedaccount").value = "";

    let inputSplitted = infosusersInput.split('\n');

    inputSplitted = inputSplitted.filter(
        e => 
        {
            let infosSplitted = e.split(' ');
            if (infosSplitted.length == 2)
            {
                for (let i = 0; i < 2; i++)
                {
                    if ((!infosSplitted[i] || infosSplitted[i] == " " || infosSplitted[i] == "" || infosSplitted[i].length == 0))
                    {
                        return false;
                    }
                }
                return true;
            }

            return false;
        }
    );

    $.ajax({
        url : 'infosuser_query.php',
        type : 'POST', // Le type de la requête HTTP, ici devenu POST
        data: { users: JSON.stringify(inputSplitted)},
        dataType : 'text',
        success : function(code_html)
        {
            displayResult(code_html);
        },
        error : () => displayResult('danger Impossible d\'accéder à la page de requête !')
    });
}

function displayResult(result)
{
    let group_msg_received = result.split('|');
    for (let i = 0; i < group_msg_received.length; i++)
    {
        let msg_received_split = group_msg_received[i].split(' ');

        if (msg_received_split.length > 1)
        {
            let status = msg_received_split[0];
            msg_received_split.shift();

            group_msg_received[i] = group_msg_received[i].substring(status.length + 1);

            if (status == "data_success") // Affiche les comptes crées
            {
                middleTextArea = document.getElementById("successaccount");
                middleTextArea.value += group_msg_received[i];
                middleTextArea.value += '\r';
            }
            else if (status == "data_failed") // Affiche les erreurs
            {
                rightTextArea = document.getElementById("failedaccount");
                rightTextArea.value += group_msg_received[i];
                rightTextArea.value += '\r';
            }
            else // Affiche le message
            {
                stateElement = document.getElementById("msg_state");
                stateElement.className = "alert alert-dismissible alert-" + status;
                stateElement.innerText = group_msg_received[i];
                
                document.getElementsByName("state_block")[0].hidden = false;
            }
        }
    }
}