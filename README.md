Site web développé en PHP permettant :
- La création d'utilisateur (Elève, Professeur et Administrateur)
- Création de devoir à rendre
- Rendu des devoirs
- Système de stockage (visualisation, suppression, édition, ajout) de fichiers avec droits spécifiques et paramètres modifiables
- Command line d'administration du site
- Création de compte élève en masse
- Création de classe / cours
- Assignation de cours à des classes et d'élèves à des cours

Base de données en MySQL
