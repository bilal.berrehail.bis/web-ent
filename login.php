<?php

include_once('utility/Header.php');

ManagerSession::start();

if (ManagerSession::isLogged())
{
    return; // Déjà connecté, on affiche rien
}

$state_code = -1;

$user_id = -1;

if (isset($_POST['input_username']) && isset($_POST['input_password']))
{
    $state_code = $_SESSION['instanceMysql']->checkMethodPostLogin($_POST['input_username'], $_POST['input_password'], $user_id);

    if ($state_code == AUTHENTICATION_SUCCESS)
    {
        if (!ManagerSession::setValue($user_id))
            $state_code = SESSION_ERROR;
        else
            echo "<script> window.location.href='index.php'; </script>";
    }
}

?>

<html>
    <head>
        <?php loadingHtmlNavbar::loadHeadContent(); ?>
        <link href="css/login.css" rel="stylesheet">
    </head>

    <body id="LoginForm">

        <?php loadingHtmlNavbar::loadNavbar(); ?>
        <br/>
        <?php LoadingHtmlLogin::loadLoginPageClean($state_code); ?>

    </body>
</html>
