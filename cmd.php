<?php

include_once('utility/Header.php');
ManagerSession::start();

$isLogged = ManagerSession::isLogged();

if (!$isLogged) // Il doit nécéssairement être connecté
{
    return;
}

$user_id = $_SESSION['user_id'];

$get_permissions_grade = $_SESSION['instanceMysql']->getPermissionsGradeByUserID($user_id);

if ($get_permissions_grade != 2) // Administrateur uniquement
{
    return;
}
?>

<!DOCTYPE html>

<html>
    <head>

        <?php loadingHtmlNavbar::loadHeadContent(); ?>

        <link href="css/cmd.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="js/cmd.js"></script>

    </head>

    <body>

        <?php loadingHtmlNavbar::loadNavbar(2); ?>
        <?php loadingHtmlCmd::loadingDefaultPage(); ?>

    </body>

</html>